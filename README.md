


# Dependencies
npm install base-64 --save
npm install utf8 --save
tns install typescript 
tns plugin add nativescript-ui-sidedrawer
tns plugin add nativescript-ui-listview
tns plugin add nativescript-local-notifications
tns plugin add nativescript-plugin-firebase
tns plugin add nativescript-google-mobile-ads-sdk
tns plugin add nativescript-loading-indicator
tns plugin add nativescript-purchase
tns plugin add nativescript-imagepicker
tns plugin add nativescript-admob
tns plugin add nativescript-drop-down
tns plugin add nativescript-facebook
tns plugin add nativescript-image-swipe
tns plugin add nativescript-modal-datetimepicker
tns plugin add nativescript-menu
tns plugin add nativescript-social-share
tns plugin add nativescript-videoplayer
