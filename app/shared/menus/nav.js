var Config				= require("~/config");
//let	application			= require('application');
let application 		= require("tns-core-modules/application");
var ApplicationSettings = require('application-settings');
var frameModule			= require('ui/frame');
var viewModule			= require('ui/core/view');
var Observable			= require("data/observable").Observable
let observableModule	= require("tns-core-modules/data/observable");

class NavigationViewModel
{
    constructor()
    {
        this.drawer 	= null;
        this.component	= null;
        this.page		= null;
        this.btnBack	= null;
        this.btnMenu	= null;
        this.model		= observableModule.fromObject({
			title:	'',
			show_title: true,
			OnTapButtonSearch: (args) => this.OnTapButtonSearch(args)
		});
    }
    OnLoaded(args)
    {
        this.component 		= args.object;
        //console.log('Title:' + this.component.title);
        //var viewModel 	    = new Observable();
        //viewModel.title     = this.component.title;
        this.btnBack		= viewModule.getViewById(this.component, 'btn-back');
        this.btnMenu		= viewModule.getViewById(this.component, 'btn-menu');
        if( !this.component.showback )
        {
            this.btnBack.style.visibility = 'collapse';
        }
        else
        {
            this.btnMenu.style.visibility = 'collapse';
        }
        this.component.bindingContext = this.model;
    }
    ShowSideDrawer(args)
    {
		const mainView = application.getRootView();
		//sideDrawer.showDrawer();
        //this.page 	= frameModule.topmost().currentPage;
        this.drawer	= mainView.getViewById("sideDrawer");
        if( this.drawer )
			this.drawer.toggleDrawerState();
    }
    OnBackTap(args)
    {
        //console.log('__BACK__');
        frameModule.topmost().goBack();
    }
    OnTapButtonSearch(args)
    {
		let nav = this.component.getViewById('navigation');
		let searchBox = this.component.getViewById('search-box');
		nav.visibility = 'collapse';
		searchBox.visibility = 'visible';
		/*
		nav.animate({ translate: {x: 0, y: -50}, duration: 500, iterations: 1 }).then( () =>
		{
			//searchBox.visibility = 'visible';
		});
		//searchBox.animate({ translate: {x: 0, y: -50}, duration: 500, iterations: 1 });
		*/
	}
}
let ctrl = new NavigationViewModel();
exports.loaded          = (args) => ctrl.OnLoaded(args);
exports.showSideDrawer  = (args) => ctrl.ShowSideDrawer(args);
exports.OnBackTap       = (args) => ctrl.OnBackTap(args);
