var Observable 			= require("data/observable").Observable;
var Config				= require("~/config");
var ApplicationSettings = require('application-settings');
let application 		= require("tns-core-modules/application");
var frameModule			= require('ui/frame');
var viewModule			= require('ui/core/view');
let observableModule 	= require("data/observable");
//let camera              = require('nativescript-camera');
let imageModule         = require('ui/image');
//let imagepicker         = require("nativescript-ssi-imagepicker");
let dialogs             = require('ui/dialogs');
let imageSource         = require('image-source');
let fileSystem          = require('file-system');
//let usersService        = require('~/modules/users/UsersService').UsersService.GetInstance();

class DrawerMenu
{
	constructor()
	{
		//#get user data from settings
	
		//console.log('JSON USER', json_user);
		this.page				= null;
		this.mainView			= null;		
		this.model 				= observableModule.fromObject({
			selectedPage:	'',
			welcome_message: '',
			user: observableModule.fromObject(
			{
				username: 	'[username]',
				email: 		'[user@mail.com]',
				avatar:		'~/images/nobody.png',
				display_name: ''
			}),
			OnTapAvatar: (args) => this.OnTapAvatar(args),
			OnNavigationItemTap: (args) => this.OnNavigationItemTap(args),
			OnLogout: (args) => this.OnLogout(args)
		});
	}
	OnLoaded(args)
	{
		this.page 				= args.object;
		this.mainView			= application.getRootView();
		//this.sideDrawer			= this.mainView.getViewById('sideDrawer');
		this.userAvatar			= this.page.getViewById('user-avatar');	
		
		//~ console.log('MAIN MENU USER');
		//~ console.log(spokaneUser);
		this.model.welcome_message 		= L('Hello');// + user.firstName + ' ' + user.lastName;
		this.model.user.email			= spokaneUser.user_email;
		this.model.user.username		= spokaneUser.user_email;
		this.model.user.display_name	= spokaneUser.user_display_name;
		this.model.user.avatar			= spokaneUser.picture ? spokaneUser.picture : this.model.user.avatar;
		this.page.bindingContext 	= this.model;
	}
	OnNavigationItemTap(args)
	{
		let menuItem 	= args.object;
		let route		= menuItem.route;
		if( !route )
			return false;
		console.log(route);
		try
		{
			this.GetDrawer().toggleDrawerState();
			menuItem.page.frame.navigate({
		        moduleName: route,
		        transition: {
		            name: "fade"
		        },
		        animated: true
			});
		}
		catch(e)
		{
			console.dir(e);
			alert({title: 'Spokane News',
				message: L('Unknow error, please contact with support'),
				okButtonText: L('Close')
			});
		}
	}
	OnLogout()
	{
		this.GetDrawer().toggleDrawerState();
        spk_close_session();
        //console.log(ApplicationSettings.getBoolean("authenticated", false));
        //console.log('__SESSION_CLOSED__');
		frameModule.topmost().navigate({
		        moduleName: 'modules/login/login',
		        transition: {
		            name: "fade"
		        }
		});
	}
    OnTapAvatar(args)
    {
		args.object.page.frame.navigate({
			moduleName: 'modules/profile/profile'
		});
		/*
        dialogs.action({
            message:            L('Select an option'),
            cancelButtonText:   L('Cancel'),
            actions:            [L('Take a photo'), L('Select from my gallery')]
        })
        .then( (res) => 
        {
            if( res == L('Take a photo'))
            {
                this.UserCamera();
            }
            else if( res == L('Select from my gallery') )
            {
                this.SelectFromGallery();
            }
        });
        */
    }
    UserCamera()
    {
        camera.takePicture().then( (imageAsset) =>
        {
            this.userAvatar.src = imageAsset;
        })
        .catch( (err) =>
        {
            console.log("Error -> " + err.message);
        });
    }
    SelectFromGallery()
    {
        let context = imagepicker.create({mode: 'single'});
        context
            .authorize()
            .then( () =>
            {
                return context.present();
            })
            .then( (selection) =>
            {
                selection.forEach((selected)  =>
                {                   
                    selected.getImage({maxWidth: 350, maxHeight: 350}).then( (imageSource) => 
                    {
                        this.userAvatar.imageSource = imageSource;
                        this.UpdateUserAvatar();
                    });
                });
                
            })
            .catch( (e) => 
            {
                // process error
                alert(e);
            });
    }
    UpdateUserAvatar()
    {
        if( !this.userAvatar.imageSource )
            return false;
        try
        {
            let buffer = this.userAvatar.imageSource.toBase64String("jpg");
            usersService.UpdateAvatar(user, buffer).then( (res) => 
            {
            },
            (error) => 
            {
                console.dir(error);
            });
        }
        catch(e)
        {
            alert({message: 'AVATAR ERROR: ' + e});
        }
    }
    GetDrawer()
    {
		this.mainView = application.getRootView();
		return this.mainView.getViewById('sideDrawer');
	}
}
let ctrl = new DrawerMenu();
exports.loaded = (args) => ctrl.OnLoaded(args);
