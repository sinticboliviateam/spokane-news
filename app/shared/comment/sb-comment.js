const builder	= require("tns-core-modules/ui/builder");
const view 		= require("tns-core-modules/ui/core/view");
const GridLayoutModule	= require('tns-core-modules/ui/layouts/grid-layout');
const StackLayout		= require('tns-core-modules/ui/layouts/stack-layout').StackLayout;
const observableModule	= require('tns-core-modules/data/observable');
const Label				= require('tns-core-modules/ui/label').Label;

class SBCommentBox
{
}
class SBComment extends StackLayout //GridLayoutModule.GridLayout
{
	constructor()
	{
		super();
		let viewFile		= __dirname + '/sb-comment.xml';
		this.viewComponent 	= builder.load(__dirname + '/sb-comment.xml', null);
		this.model 			= observableModule.fromObject({
			comment: observableModule.fromObject({
				author: observableModule.fromObject({
					name: '',
					image: ''
				}),
				content: '',
				date_time: ''
			}),
			isHtml: false,
			showActions: false,
			showEdit: false,
			showDelete: false,
			OnLoaded: (args) => this.OnLoaded(args),
			//creatingViewContent: (args) => this.creatingViewContent(args),
			OnTapEdit: null,
			OnTapDelete: null,
			css: '<style>img{max-width:50%;max-height:150px;}</style>'
		});
		this.viewComponent.bindingContext = this.model;
        this.addChild(this.viewComponent);
       
	}
	SetComment(obj)
	{
		if( obj.author.image && obj.author.image.indexOf('http') == -1 )
		{
			//console.log('DETECTED BASE64 IMAGE');
			//let buffer = obj.author.image;
			const ImageSourceModule = require('tns-core-modules/image-source');
			//let handler = new ImageSourceModule.ImageSource();
			obj.author.image = ImageSourceModule.fromBase64(obj.author.image);
			//~ handler.fromBase64(buffer).then( (img) =>
			//~ {
				//~ this.model.comment.author.image = img;
			//~ });
			//~ obj.author.image = '';
		}
		Object.assign(this.model.comment, obj);
		//console.dir(this.model.comment);
		if( this.model.isHtml )
		{
			this.model.comment.content = this.model.css + this.model.comment.content.trim();
		}
	}
	OnLoaded(args)
	{
		
	}
	creatingViewContent(args)
	{
		if( !this.model.isHtml )
			return false;
			
		const HtmlView = require("tns-core-modules/ui/html-view").HtmlView;
		let content = new HtmlView();
		content.html = this.model.comment.content;
		args.view = content;
	}
	SetEditEvent(callback)
	{
		this.model.OnTapEdit = callback;
	}
	SetDeleteEvent(callback)
	{
		this.model.OnTapDelete = callback;
	}
}

exports.SBComment 		= SBComment;
exports.SBCommentBox	= SBCommentBox;
