const Config		= require('~/config');
const BaseApi	= require('~/shared/api');
let $instance 	= null;

class ApiWp extends BaseApi.ApiSpokane
{
	constructor()
	{
		super();
		this.baseURL = Config.BASEURL + '/../wp-json/wp/v2';
	}
	Register(data)
	{
		let endpoint = '/users/register';
		return new Promise( (resolve, reject) =>
		{
			this.post(endpoint, data).then(resolve, reject);
		});
	}
	Login(credentials)
	{
		if( !credentials.username )
			throw {error: 'Invalid username'};
		if( !credentials.password )
			throw {error: 'Invalid password'};
		let endpoint = '/../../jwt-auth/v1/token';
		return new Promise( (resolve, reject) => {
			this.post(endpoint, credentials).then(resolve, reject);
		});
	}
	UploadAttachment(attach)
	{
		if( !attach )
			throw {error: 'Invalid attachment'};
		if( !attach.filename )
			throw {error: 'Invalid attachment filename'};
		if( !attach.imageBase64 )
			throw {error: 'Invalid attachment buffer data'};
			
		return new Promise( (resolve, reject) =>
		{
			let endpoint = '/../../spokanews/v1/image';
			this.post(endpoint, attach).then(resolve, reject);
		});
	}
	UpdateUser(user)
	{
		if( !user.ID )
			throw {error: 'Invalid user identifier'};
		let endpoint = '/users/' + user.ID;

		return new Promise( (resolve, reject) =>
		{
			this.put(endpoint, user).then(resolve, reject);
		});
	}
	CreatePost(post)
	{
		let endpoint = '/posts';
		return new Promise( (resolve, reject) => {
			this.post(endpoint, post).then(resolve, reject);
		});
	}
	ReadPost(id)
	{
		let endpoint = '/posts/' + id;
		return new Promise( (resolve, reject) => {
			this.get(endpoint, post).then(resolve, reject);
		});
	}
	ReadPosts(args)
	{
		let endpoint = '/posts';
		try
		{
			endpoint += '?page=' + (args.page || 1);
			endpoint += '&per_page=' + (args.limit || 100);
			for(let key in args)
			{
				if( key == 'page' || key == 'limit' )
					continue;
				let value = args[key]
				endpoint += `&${key}=${value}`;
			}
		}
		catch(e)
		{
			console.log(e);
		}
		
		//console.log(endpoint);
		return new Promise( (resolve, reject) => {
			this.get(endpoint).then(resolve, reject);
		});
	}
	UpdatePost(post)
	{
		if( !person.ID )
			throw {error: 'Invalid person identifier'};
			
		let endpoint = this.baseURL + '/posts/' + post.ID;
		return new Promise( (resolve, reject) => {
			this.put(endpoint, post).then(resolve, reject);
		});
	}
	CreateCustomPost(post, post_type)
	{
		let endpoint = '/' + post_type;
		return new Promise( (resolve, reject) => {
			this.post(endpoint, post).then(resolve, reject);
		});
	}
	ReadCustomPost(id, post_type)
	{
		let endpoint = '/' + post_type + '/' + id;
		return new Promise( (resolve, reject) => {
			this.get(endpoint).then(resolve, reject);
		});
	}
	ReadCustomPosts(post_type, args)
	{
		let endpoint = '/' + post_type;
		endpoint += '?page=' + (args.page || 1);
		endpoint += '&per_page=' + (args.limit || 100);

		if( args )
		{
			for(let key in args)
			{
				if( key == 'page' || key == 'limit' )
					continue;
				endpoint += `&${key}=${args[key]}`;
			}
		}
		return new Promise( (resolve, reject) => {
			this.get(endpoint).then(resolve, reject);
		});
	}
	UpdateCustomPost(post, post_type)
	{
		if( !post.ID && !post.id )
			throw {error: 'Invalid custom post \'${post_type}\' identifier'};
		post.id = post.id || post.ID;

		let endpoint = `/${post_type}/${post.id}`;
		return new Promise( (resolve, reject) => {
			this.put(endpoint, post).then(resolve, reject);
		});
	}
	DeleteCustomPost(id, post_type)
	{
		if( !id )
			throw {error: 'Invalid custom post \'${post_type}\' identifier'};
			
		let endpoint = `/${post_type}/${id}`;
		return new Promise( (resolve, reject) => {
			this.delete(endpoint).then(resolve, reject);
		});
	}
	CreateComment(pid, comment, parent)
	{
		if( !pid )
			throw {error: 'Invalid post identifier'};
			
		let wpComment = {
			//author:			0,
			//author_email:	'',
			//author_name:	'',
			//author_url:		'',
			content:		comment,
			parent:			parent || 0,
			post:			pid,
			//status:			'pending',
			meta:			{}
		};
		let endpoint = `/comments`;
		return new Promise( (resolve, reject) =>
		{
			this.post(endpoint, wpComment).then(resolve, reject);
		});
	}
	ReadComment(id)
	{
		let endpoint = `/comments/${id}`;
		return new Promise( (resolve, reject) =>
		{
			this.get(endpoint).then(resolve, reject);
		});
	}
	ReadAllComments(pid, page, limit)
	{
		page	= page <= 0 ? 1 : page;
		limit 	= limit || 50;
		
		let endpoint = `/comments?page=${page}&per_page=${limit}&order=asc&orderby=date_gmt`;
		
		if( pid )
			endpoint += `&post=${pid}`;

		return new Promise( (resolve, reject) =>
		{
			this.get(endpoint).then(resolve, reject);
		});
	}
	DeleteComment(id)
	{
		if( !id )
			throw {error: 'Invalid comment identifier'};
			
		let endpoint = `/comments/${id}`;
		return new Promise( (resolve, reject) =>
		{
			this.delete(endpoint + '?force=1').then(resolve, reject);
		});
	}
	CreateMissingPerson(person)
	{
		if( person.image )
		{
			return new Promise( (resolve, reject) =>
			{
				this.UploadAttachment(person.image).then( (response) =>
				{
					person.image = null;
					delete person.image;
					let res = response.content.toJSON();
					
					person.featured_media = res.ID;
					this.CreateCustomPost(person, 'missing_person').then(resolve).catch(reject);
				});
			});
			
		}
		return this.CreateCustomPost(person, 'missing_person');
	}
	ReadMissingPerson(id)
	{
		return this.ReadCustomPost(id, 'missing_person');
	}
	ReadMissingPersons()
	{
		return this.ReadCustomPosts('missing_person');
	}
	UpdateMissingPerson(person)
	{
		if( person.image )
		{
			return new Promise( (resolve, reject) =>
			{
				this.UploadAttachment(person.image).then( (response) =>
				{
					person.image = null;
					delete person.image;
					let res = response.content.toJSON();
					
					person.featured_media = res.ID;
					this.UpdateCustomPost(person, 'missing_person').then(resolve).catch(reject);
				});
			});
			
		}
		return this.UpdateCustomPost(person, 'missing_person');
	}
	DeleteMissingPerson(id)
	{
		return this.DeleteCustomPost(id, 'missing_person');
	}
	CreateLostPet(pet)
	{
		if( pet.image )
		{
			return new Promise( (resolve, reject) =>
			{
				this.UploadAttachment(pet.image).then( (response) =>
				{
					pet.image = null;
					delete pet.image;
					let res = response.content.toJSON();
					pet.featured_media = res.ID;
					this.CreateCustomPost(pet, 'lostpet').then(resolve).catch(reject);
				});
			});
			
		}
		return this.CreateCustomPost(pet, 'lostpet');
	}
	UpdateLostPet(pet)
	{
		if( pet.image )
		{
			return new Promise( (resolve, reject) =>
			{
				this.UploadAttachment(pet.image).then( (response) =>
				{
					pet.image = null;
					delete pet.image;
					let res = response.content.toJSON();
					
					pet.featured_media = res.ID;
					this.UpdateCustomPost(pet, 'lostpet').then(resolve).catch(reject);
				});
			});
			
		}
		return this.UpdateCustomPost(pet, 'lostpet');
	}
	static GetInstance()
	{
		if( $instance )
			return $instance;
		$instance = new ApiWp();
		
		return $instance;
	}
}
exports.ApiWp		= ApiWp;
exports.GetInstance = ApiWp.GetInstance;
