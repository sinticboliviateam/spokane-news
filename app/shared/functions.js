require('globals');
const application 			= require("tns-core-modules/application");
const frame					= require('tns-core-modules/ui/frame');
const LocalNotifications 	= require('nativescript-local-notifications').LocalNotifications;
const firebase 				= require("nativescript-plugin-firebase");
const applicationResources 	= application.getResources();
const LoadingIndicator		= require('nativescript-loading-indicator').LoadingIndicator;
const admob 				= require("nativescript-admob");
const purchase				= require('nativescript-purchase');
const ApplicationSettings	= require('application-settings');
const facebookModule 		= require("nativescript-facebook");

function spk_setup_purchases()
{
	//##setup purchase products
	purchase.init(appSettings.products).then( () =>
	{
		//console.log('getting products');
		purchase.getProducts().then( (products) =>
		{
			let str = '';
			for(let prod of products)
			{
				str += `Product: ${prod.productIdentifier}, ${prod.localizedTitle}, ${prod.priceFormatted}`;
				spokaneProducts[prod.productIdentifier] = prod;
			}
			//alert({title: L('App Products Found'), message: str, okButtonText: L('Close')});
		})
		.catch( (e) =>
		{
			//alert({title: L('ERROR Application Products'), message: L('Unable to get app purchase products'), okButtonText: L('Close')});
			console.log('ERROR getting products');
			console.dir(e);
		});
	});
}
function spk_normalize_fb_message()
{
	
}
function spk_setup_firebase()
{
	firebase.init({
		// Optionally pass in properties for database, authentication and cloud messaging,
		// see their respective docs.
		showNotifications: true,
		showNotificationsWhenInForeground: true,
		onPushTokenReceivedCallback: (token) =>
		{
			console.log("FIREBASE: Firebase push token Received: " + token);
			spk_set_firebase_token(token);
		},
		onMessageReceivedCallback: (message) =>
		{
			console.log('FIREBASE: onMessageReceivedCallback at => ' + sb_formatdate(new Date()));
			console.log(message);
			let notify = [];
			let allWaysActions = ['alert', 'notify', 'post', 'close_session', 'clear_data'];
			if( message.data && message.data.type && allWaysActions.indexOf(message.data.type) != -1 )
			{
				if( message.data.type == 'close_session' )
				{
					spk_close_session();
					frame.topmost().navigate({moduleName: 'modules/login/login', clearHistory: true});
				}
				//~ else
				//~ {
					//~ notify.push({
						//~ id: 			`${message.from}_` + new Date().getTime(),
						//~ title: 			message.data.title,
						//~ body: 			message.data.body,
						//~ at: 			new Date(new Date().getTime() + 1 * 1000),
						//~ //groupedMessages: ["The first", "Second", "Keep going", "one more..", "OK Stop"], // android only
						//~ groupSummary: 	message.data.body, // android only
						//~ sound: 			true,
						//~ icon: 			"res://icon-45x45",//'res://ic_stat_smiley',
						//~ thumbnail: 		"res://icon-45x45",
						//~ data: 			message.data || null
					//~ });
				//~ }
			}
			else
			{
				if( message.foreground )
				{
					let currentPage = frame.topmost().currentPage;
					console.log(`Current Page: ${currentPage}`);
					
					if(message.data.type == 'news' && typeof global.NotifyNewsRefresh != 'undefined' )
					{
						console.log('Refreshing latest news');
						NotifyNewsRefresh();
					}
					
				}
				else
				{
					//~ notify.push({
						//~ id: 			`${message.from}_` + new Date().getTime(),
						//~ title: 			message.data.title,
						//~ body: 			message.data.body,
						//~ at: 			new Date(new Date().getTime() + 1 * 1000),
						//~ //groupedMessages: ["The first", "Second", "Keep going", "one more..", "OK Stop"], // android only
						//~ groupSummary: 	message.data.body, // android only
						//~ sound: 			true,
						//~ icon: 			"res://icon-45x45",//'res://ic_stat_smiley',
						//~ thumbnail: 		"res://icon-45x45",
						//~ data: 			message.data || null
					//~ });
					spk_process_firebase_message(message);
				}
			}
			if( notify.length > 0 )
			{
				LocalNotifications.schedule(notify)
					.then(() => {})
					.catch(error => console.log("doScheduleId4WithCustomIcon error: " + error));
			}
		}
	}).then(
		function ()
		{
			console.log("FIREBASE: firebase.init done");
			spk_renew_firebase_token();
		},
		function (error)
		{
			console.log("FIREBASE: firebase.init error: " + error);
		}
	);
}
function spk_renew_firebase_token()
{
	firebase.getCurrentPushToken().then((token) =>
	{
		spk_set_firebase_token(token);
	});
}
function spk_set_firebase_token(token)
{
	console.log(`FIREBASE: Setting up push token: ${token}`);
	ApplicationSettings.setString('firebase_token', token);
	//##send firebase token
	apiWp.UpdateUser({ID: spokaneUser.id, meta_data: {'_firebase_token': token}});
	firebase.subscribeToTopic("facebook").then(() => console.log("FIREBASE: Subscribed to facebook topic"));
}
function spk_setup_notifications()
{
	LocalNotifications.requestPermission().then((granted) =>
	{
		if(granted) 
		{
			console.log('NOTIFICATION: PERMISSON GRANTED');
		}
	});
	
	//##prepare local notifications
	LocalNotifications.addOnMessageReceivedCallback(spk_process_firebase_message);
}
function spk_process_firebase_message(notification)
{
	console.log('NOTIFICATION: TAP');
	console.log(notification);
	
	if( notification.data && notification.data.type )
	{
		let frameModule	= require('ui/frame');
		let $route		= null;
		let $context	= null;
		switch(notification.data.type)
		{
			case 'alert':
				alert({
					title: notification.data.title || L('Alert'),
					message: notification.data.body,
					okButtonText: L('Close')
				});
			break;
			case 'notify':
				$route = 'modules/notifications/notify';
				$context = {item: notification.data};
			break;
			case 'post':
				$route = 'modules/notifications/post';
				$context = {item: notification.data};
			break;
			case 'news':
				let action	= notification.data.action || 'refresh';
				let post	= notification.data.post || null;
				if( action == 'refresh' )
				{
					$route		= 'modules/home/home';
					$context 	=  {action: 'refresh', post: post, tab: 'latestnews'};
				}
				else if( action == 'open' )
				{
					//NotifyNewsRefresh();
					$route		= 'modules/latestnews/single';
					$context	= {id: notification.data.id};
				}
			break;
		}
		if( notification.foreground && $route )
		{
			frameModule.topmost().navigate({
				moduleName: $route,
				context: $context,
				animated: true,
				clearHistory: true
			});
		}
		else if( $route ) 
		{
			global.initNav = {route: $route, context: $context};
		}
	}
	
}
function spk_start_session(user)
{
	global.spokaneUser = user;
	ApplicationSettings.setBoolean("authenticated", true);
	ApplicationSettings.setString("token", user.token ? user.token : '');
	ApplicationSettings.setString("user", JSON.stringify(user));
	spk_renew_firebase_token();
}
function spk_close_session()
{
	console.log('__CLOSING_SESSION__');
	console.log(ApplicationSettings.getBoolean("authenticated", false));
	ApplicationSettings.setBoolean("authenticated", false);
	ApplicationSettings.remove("authenticated");
	ApplicationSettings.remove("token");
	ApplicationSettings.remove("user");
	global.spokaneUser = null;
	if( ApplicationSettings.getString('fb_token', false) )
	{
		ApplicationSettings.remove("fb_token");
		facebookModule.logout();
	}
}
global.spk_setup_purchases 		= spk_setup_purchases;
global.spk_setup_notifications 	= spk_setup_notifications;
global.spk_setup_firebase 		= spk_setup_firebase;
global.spk_start_session		= spk_start_session;
global.spk_close_session		= spk_close_session;


function sbShowProgress(msg)
{
	/*
	let modal = $page.showModal(
		'shared/modals/working',
		{message: msg},
		function(){},
		true
	);
	return modal;
	*/
	spokaneLoading.show({message: msg});
}
function sb_hide_progress()
{
	spokaneLoading.hide();
}
function sub_str(str, limit)
{
	return str.substr(0, limit);
}
function sb_get_version(args)
{
    var page = args.object;
    if(page.android)
    {
        var PackageManager = android.content.pm.PackageManager;
        var pkg = application.android.context.getPackageManager().getPackageInfo(application.android.context.getPackageName(), PackageManager.GET_META_DATA);
        alert({title: "Version", message: java.lang.Integer.toString(pkg.versionCode), okButtonText: "Close"});
    }
    else
    {
        var version = NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleShortVersionString");
        alert({title: "Version", message: version, okButtonText: "Close"});
    }
}
function sb_striptags(str)
{
	return str.replace(/(<([^>]+)>)/ig, '');
}
function sb_n2br(str, times)
{
	times = times || 1;
	let rep = '';
	for(let i = 0; i < times; i++)
	{
		rep += '<br/>';
	}
	return str.replace(/\n+/g, rep);
}
function sb_formatgmtdate(dateStr, cFormat)
{
	if( dateStr.indexOf('+') == -1 )
		dateStr += '+0000';
	let gmtDate = (dateStr instanceof Date) ? dateStr : new Date(dateStr);

	return sb_formatdate(gmtDate.toString(), cFormat);
}
function sb_formatdate(dateStr, cFormat)
{
	let monthNames = [
		"January", "February", "March",
		"April", "May", "June", "July",
		"August", "September", "October",
		"November", "December"
	];
	let format = cFormat || 'Y-m-d H:i:s';
	//console.log(`date formar => ${format}`);
	let d = (dateStr instanceof Date) ? dateStr : new Date(dateStr);

	let month 		= parseInt(d.getMonth()) + 1;
	let day 		= parseInt(d.getDate());
	let hours 		= parseInt(d.getHours());
	let singleHours = hours % 12;
	let minutes 	= parseInt(d.getMinutes());
	let seconds 	= parseInt(d.getSeconds());
	let meridiem 	= (hours > 11) ? 'pm' : 'am';
	
	if( month < 10 )
		month = '0' + month;
	if( day < 10 )
		day = '0' + day;
		
	if( hours < 10 )
		hours = '0' + hours;
		
	if( minutes < 10 )
		minutes = '0' + minutes;

	if( seconds < 10 )
		seconds = '0' + seconds;
	let fullYear = d.getFullYear();
		
	let date = format.replace(/([a-zA-Z])/g, '{$1}')
				.replace('{Y}', fullYear)
				.replace('{m}', month)
				.replace('{M}', monthNames[parseInt(month) - 1])
				.replace('{d}', day)
				.replace('{H}', hours)
				.replace('{h}', singleHours)
				.replace('{i}', minutes)
				.replace('{s}', seconds)
				.replace('{T}', meridiem);
						
	return date;
}
function sb_replace_html_numbers(str)
{
	return str.replace(/&#038;/g, '&');
}
function sb_entitydecode(str)
{
	//return str.replace(/&#([0-9]{1,4});/gi, function(match, numStr)
	return str.replace(/&#(\d+);/gi, function(match, numStr)
	{
        var num = parseInt(numStr, 10); // read num as normal number
        return String.fromCharCode(num);
    });
}
function sb_admob_banner($keys)
{
	if( spokaneUser && spokaneUser.user_type != 'free' )
		return true;
		
	let keywords = $keys || spokaneAdMob.keywords;
	admob.createBanner({
		// if this 'view' property is not set, the banner is overlayed on the current top most view
		//view: 'admob-item',
		testing: spokaneAdMob.testing, // set to false to get real banners
		size: admob.AD_SIZE.SMART_BANNER, // anything in admob.AD_SIZE, like admob.AD_SIZE.SMART_BANNER
		iosBannerId: spokaneAdMob.bannerId, // add your own
		androidBannerId: spokaneAdMob.bannerId, // add your own
		// Android automatically adds the connected device as test device with testing:true, iOS does not
		iosTestDeviceIds: ["yourTestDeviceUDIDs", "canBeAddedHere"],
		margins: {
		  // if both are set, top wins
		  //top: 10
		  bottom: 50
		},
		keywords: keywords // add keywords for ad targeting
	}).then(
		() =>
		{
			console.log("admob createBanner done");
		},
		(error) =>
		{
			console.log("admob createBanner error: " + error);
		}
	);
}
let admobLoaded = false;
let admobPromise = null;
function sb_admob_interstitial()
{
	if( spokaneUser.user_type != 'free' )
		return true;
	let args = {
		testing: spokaneAdMob.testing,
		iosInterstitialId: spokaneAdMob.interstitial, // add your own
		androidInterstitialId: spokaneAdMob.interstitial, // add your own
		// Android automatically adds the connected device as test device with testing:true, iOS does not
		iosTestDeviceIds: ["ce97330130c9047ce0d4430d37d713b2"],
		keywords: spokaneAdMob.keywords, // add keywords for ad targeting
		onAdClosed: () => { console.log("interstitial closed") }
	};
	console.log('LOADING ADMOB INTERSTITIAL... --> ' + spokaneAdMob.interstitial);
	//~ if( admobLoaded )
	//~ {
		//~ admobPromise.then( () => {admob.showInterstitial();});
		//~ admob.showInterstitial();
		//~ return true;
	//~ }
	
	admob.preloadInterstitial(args).then(
		() =>
		{
			admobLoaded = true;
			admob.showInterstitial();	
		},
		(error) =>
		{
			console.log("admob preloadInterstitial error: " + error);
		}
	);
}
function sb_object2querystring(obj)
{
	return Object.keys(obj).reduce(function (str, key, i)
	{
		let delimiter, val;
		delimiter = (i === 0) ? '?' : '&';
		key = encodeURIComponent(key);
		val = encodeURIComponent(obj[key]);
		return [str, delimiter, key, '=', val].join('');
	}, '');
}
//applicationResources.sbShowProgress = sbShowProgress;
applicationResources.sub_str = sub_str;
applicationResources.sb_striptags = sb_striptags;
applicationResources.sb_formatdate = sb_formatdate;
applicationResources.sb_formatgmtdate = sb_formatgmtdate;
applicationResources.sb_replace_html_numbers = sb_replace_html_numbers;
applicationResources.sb_entitydecode = sb_entitydecode;
applicationResources.sb_n2br = sb_n2br;
application.setResources(applicationResources);
global.sbShowProgress 	= sbShowProgress;
global.sb_show_progress = sbShowProgress;
global.sb_hide_progress	= sb_hide_progress;
global.sub_str			= sub_str;
global.sb_get_version	= sb_get_version;
global.spokaneLoading	= new LoadingIndicator();
global.sb_striptags		= sb_striptags;
global.sb_n2br			= sb_n2br;
global.sb_formatdate	= sb_formatdate;
global.sb_formatgmtdate	= sb_formatgmtdate;
global.sb_replace_html_numbers = sb_replace_html_numbers;
global.sb_entitydecode	= sb_entitydecode;
global.sb_admob_banner	= sb_admob_banner;
global.sb_admob_interstitial = sb_admob_interstitial;
global.spk_renew_firebase_token = spk_renew_firebase_token;
global.spk_set_firebase_token	= spk_set_firebase_token;
global.sb_object2querystring	= sb_object2querystring;
