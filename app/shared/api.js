var Config 	= require('~/config');
var http	= require('http');
var ApplicationSettings = require('application-settings');
let $instance = null;

class ApiSpokane
{
	constructor()
	{
		this.baseURL 		= Config.BASEURL;
		this.headers		= {};
		this.contentType	= 'json';
	}
	GetHeaders()
	{
		let headers = {};
		if( this.contentType == 'json' )
		{
			headers = 
			{
				//'Accept': 'application/json', 
				'Content-Type': 'application/json'
			};
		}
        let token = ApplicationSettings.getString('token');
        if( token )
        {
            headers['Authorization'] = 'Bearer ' + token;
        }

        return Object.assign(this.headers, headers);
	}
	GetXHR(endpoint, method, data = null)
	{
		var cfg = 
		{
			url: 		this.baseURL + endpoint, 
			method:		method.toUpperCase(), 
			headers: 	this.GetHeaders()
		};
		
		if( (method == 'POST' || method == 'PUT') && data )
		{
			cfg.content = this.contentType == 'json' ? JSON.stringify(data) : data;
		}
		//console.log(cfg);
		return http.request(cfg);
	}
	get(endpoint)
	{
		return this.GetXHR(endpoint, 'GET', null);
	}
	post(endpoint, data)
	{
		return this.GetXHR(endpoint, 'POST', data);
	}
	put(endpoint, data)
	{
		return this.GetXHR(endpoint, 'PUT', data);
	}
	delete(endpoint)
	{
		return this.GetXHR(endpoint, 'DELETE', null);
	}
	static GetInstance()
	{
		if( $instance )
			return $instance;
		$instance = new ApiSpokane();
		
		return $instance;
	}
}
exports.ApiSpokane 	= ApiSpokane;
exports.api 		= ApiSpokane.GetInstance();
