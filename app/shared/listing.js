let frame 				= require('tns-core-modules/ui/frame');
let observableModule 	= require('tns-core-modules/data/observable');
const ObservableArray	= require('tns-core-modules/data/observable-array').ObservableArray;
let	api					= require('~/shared/api').api;

class ListingViewModel
{
	constructor()
	{
		this.page 		= null;
		this.model		= null;
		this.endpoint 	= null;
		this.singleViewModule = '';
		this.totalPages			= 0;
		this.totalItems			= 0;
		this.itemsCurrentPage 	= 0;
		this.limit				= 50;
		this.listview			= null;
		this.apiType			= 'apiWp';
		this.itemsArgs			= {};
		this.model = observableModule.fromObject({
			items:	new ObservableArray([]),
			showActivity: false,
			OnTapBack: (args) => this.OnTapBack(args),
			OnTapListItem: (args) => this.OnTapListItem(args),
			OnLoadMoreItems: (args) => this.OnLoadMoreItems(args),
			RefreshItems: (args) => this.RefreshItems(args)
		});
	}
	OnNavigatingTo()
	{
		console.log('ListingViewModel -> OnNavigatingTo');
	}
	OnLoaded(args)
	{
		this.page = args.object;
		this.page.bindingContext = this.model;
		this.listview = this.page.getViewById('listing-items');
		this.RefreshItems();
	}
	OnTapBack(args)
	{
		frame.topmost().goBack();
	}
	GetItems(page)
	{
		//console.log(`GetItems -> ${this.listview.postype}`);
		if( this.listview.postype )
		{
			let method 	= 'ReadPosts';
			let args 	= [];
			let theApi 	= apiWp;
			
			if( this.listview.postype != 'post' )
			{
				
				if( this.listview.postype == 'wc_product' )
				{
					theApi = apiWc;
					method = 'GetProducts';
				}	
				else
				{
					args.push(this.listview.postype);
					method = 'ReadCustomPosts';
				}
				
			}
			let methodArgs = {page: page, limit: this.limit};
			Object.assign(methodArgs, this.itemsArgs || {});
			args.push(methodArgs);
			//console.log(args);
			return new Promise((resolve, reject) =>
			{
				this.model.showActivity = true;
				theApi[method].apply(theApi, args)
					.then( (response) =>
					{
						
						this.model.showActivity = false;
						if( response.statusCode != 200 )
							throw response.content.toJSON();
						this.itemsCurrentPage = page;
						this.totalItems		= parseInt(response.headers['X-WP-Total']);
						this.totalPages		= parseInt(response.headers['X-WP-TotalPages']);
						resolve(response.content.toJSON());
						
					})
					.catch( (e) =>
					{
						console.log(e);
						alert({
							title: L('Listing Error'),
							message: e.message || e.error || L('Unknow error ocurred while trying to get listing items'),
							okButtonText: L('Close')
						});
						
						reject(e);
					});
			});
		}
		
		let theapi = this.apiType == 'apiWp' ? apiWp : this.apiType == 'apiWc' ? apiWc : api;
		let params = '';
		if( this.apiType == 'apiWp' || this.apiType == 'apiWc' )
		{
			params = 'page=' + page;
		}
		let endpoint = this.endpoint + '?' + params;
		
		return new Promise( (resolve, reject) =>
		{
			this.model.showActivity = true;
			theapi.get(endpoint).then( (response) =>
			{
				//console.log(response);
				this.model.showActivity = false;
				if( response.statusCode != 200 )
					throw response.content.toJSON();
				this.itemsCurrentPage = page;
				
				if( this.apiType == 'apiWp' )
				{
					this.totalItems		= parseInt(response.headers['X-WP-Total']);
					this.totalPages		= parseInt(response.headers['X-WP-TotalPages']);
					resolve(response.content.toJSON());
				}
				else if( this.apiType == 'apiWc' )
				{
					resolve(response.content.toJSON());
				}
				else
				{
					resolve(response.content.toJSON().api_data);
				}
			},
			(e) =>
			{
				this.model.showActivity = false;
				//console.dir(e);
				
				alert({
					title: L('Listing Error'),
					message: e.message || e.error || L('Unknow error ocurred while trying to get listing items'),
					okButtonText: L('Close')
				});
				
				reject(e);
			});
			
		});
	}
	RefreshItems(args)
	{
		this.GetItems(1).then( (items) =>
		{
			//console.log(items);
			this.model.items 	= new ObservableArray(items);
			this.listview.notifyPullToRefreshFinished();
		});
		if( args )
			args.returnValue = true;
	}
	OnLoadMoreItems(args)
	{
		let nextPage = this.itemsCurrentPage + 1;
		if( nextPage >= this.totalPages )
		{
			this.listview.notifyLoadOnDemandFinished();
			return false;
		}
		this.GetItems(nextPage)
			.then( (items) =>
			{
				this.model.items.push(items);
				this.listview.notifyLoadOnDemandFinished();
				
			})
			.catch( (e) =>
			{
				this.listview.notifyLoadOnDemandFinished();
			});
		args.returnValue = true;
	}
	OnTapListItem(args)
	{
		//console.log(this.singleViewModule);
		if( !this.singleViewModule )
			return false;
			
		//let item = this.model.items[args.index];
		let item = this.model.items.getItem(args.index);
		if( !item )
			return false;
		
		frame.topmost().navigate({
			moduleName: this.singleViewModule,
			context: 
			{
				post: item
			},
			animated: true
		});
	}
}
exports.ListingViewModel = ListingViewModel;
