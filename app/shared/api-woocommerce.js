const BaseApi	= require('~/shared/api');
let $instance 	= null;
let base64 		= require('base-64');
let utf8 		= require('utf8');
const http		= require('http');

class ApiSpokaneWoocommerce extends BaseApi.ApiSpokane
{
	constructor()
	{
		super();
		this.wsBaseUrl		= this.baseURL;
		this.baseURL		= 'https://www.spokane-news.com/wp-json/wc/v3';
		this.consumer_key 	= null;
		this.secret_key		= null;
	}
	GetHeaders()
	{
		let headers = {};
		if( this.contentType == 'json' )
		{
			headers = 
			{
				//'Accept': 'application/json', 
				'Content-Type': 'application/json'
			};
		}
        //~ if( token )
        //~ {
            //~ headers['Authorization'] = 'Basic ' + hash;
        //~ }
        
        return Object.assign(this.headers, headers);
	}
	GetXHR(endpoint, method, data = null)
	{
		let url = this.baseURL + endpoint;
		if( url.indexOf('?') != -1 )
			url += '&';
		else
			url += '?';
		url += `&consumer_key=${this.consumer_key}&consumer_secret=${this.secret_key}`;
		var cfg = 
		{
			url: 		url, 
			method:		method.toUpperCase(), 
			headers: 	this.GetHeaders()
		};
		
		if( (method == 'POST' || method == 'PUT') && data )
		{
			cfg.content = this.contentType == 'json' ? JSON.stringify(data) : data;
		}
		//console.log(cfg);
		return http.request(cfg);
	}
	SetCredentials(ck, sk)
	{
		this.consumer_key 	= ck;
		this.secret_key		= sk;
	}
	GetProducts(args)
	{
		let endpoint = '/products';
		endpoint += '?page=' + (args.page || 1);
		endpoint += '&per_page=' + (args.limit || 100);
		endpoint += '&status=publish';
		//limit = limit ? limit : 50;
		//page = page ? page : 1;
		return new Promise( (resolve, reject) =>
		{
			this.get(endpoint).then(resolve, reject);
		});
	}
	CreateProduct(post)
	{
		if( post.image )
		{
			let imageData = post.image;
			post.image = null;
			return new Promise( (resolve, reject) =>
			{
				this.post('/../../spokanews/v1/image', imageData)
					.then( (response) =>
					{
						console.log('Image uploaded');
						try
						{
							let res = response.content.toJSON();
							//console.dir(res);
							post.images = [{id: res.ID}];
							//resolve(res);
							this.post('/products', post).then(
								(r) => {resolve(r.content.toJSON());},
								(e) => {reject(e);}
							);
						}
						catch(e)
						{
							console.log('__ERROR__');
							console.log(e);
							reject(e);
						}
						
					}, (e) => {console.dir('ERROR uploading image');reject(e);})
					
			});
			
		}
		else
		{
			return new Promise( (resolve, reject) =>
			{
				this.post('/products', post).then((response) =>
				{
					resolve(response.content.toJSON());
				},
				(e) => {reject(e);});
			});
		}
	}
	ReadProduct(productId)
	{
		return new Promise( (resolve, reject) =>
		{
			let endpoint = '/products/' + productId;
			this.get(endpoint).then(resolve, reject);
		});
	}
	UpdateProduct(product)
	{
		if( !product.id )
			throw {error: 'Invalid product identifier, unable to update'};
		if( product.image )
		{
			let imageData = product.image;
			product.image = null;
			return new Promise( (resolve, reject) =>
			{
				this.put('/../../spokanews/v1/image', imageData)
					.then( (response) =>
					{
						try
						{
							let res = response.content.toJSON();
							//console.dir(res);
							post.images = [{id: res.ID}];
							//resolve(res);
							this.put('/products/' + product.id, product).then(
								(r) => {resolve(r.content.toJSON());},
								(e) => {reject(e);}
							);
						}
						catch(e)
						{
							console.log('__ERROR__');
							console.log(e);
							reject(e);
						}
						
					}, (e) => {console.dir('ERROR uploading image');reject(e);})		
			});
			
		}
		else
		{
			return new Promise( (resolve, reject) =>
			{
				this.put('/products/' + product.id, product).then((response) =>
				{
					resolve(response.content.toJSON());
				},
				(e) => {reject(e);});
			});
		}
	}
	DeleteProduct(productId)
	{
		return new Promise( (resolve, reject) =>
		{
			this.delete('/products/' + productId + '?force=true').then(resolve, reject);
		});
	}
	static GetInstance(credentials)
	{
		if( $instance )
			return $instance;
		$instance = new ApiSpokaneWoocommerce();
		if( credentials )
			$instance.SetCredentials(credentials.consumer_key, credentials.secret_key);
		
		return $instance;
	}
}
exports.ApiSpokaneWoocommerce 	= ApiSpokaneWoocommerce;
exports.GetInstance				= ApiSpokaneWoocommerce.GetInstance;
