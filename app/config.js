var Config =
{
	BASEURL:    	'https://spokane-news.com/webservices', 
    PUSHER:     	{address: '192.168.1.7', port: 2666, path: ''},
    DEVELOPMENT:	!true
};

exports.get = function(key)
{
	return Config[key] || null;
};
for(let key in Config)
{
	exports[key] = Config[key];
}
