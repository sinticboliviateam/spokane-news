let api 		= require('~/shared/api').api;
let frameModule	= require('ui/frame');
let observableModule	= require('data/observable');
class RegisterViewModel
{
	constructor()
	{
		this.page = null;
		this.model = observableModule.fromObject({
			user: observableModule.fromObject({
				name: '',
				username: '',
				email: '',
				password: '',
				rpassword: '',
				phone: ''
			}),
			OnTapBack: (args) => this.OnTapBack(args),
			OnTapRegister: (args) => this.OnTapRegister(args)
		});
	}
	OnLoaded(args)
	{
		this.page = args.object;
		this.page.bindingContext = this.model;
	}
	OnTapBack(args)
	{
		frameModule.topmost().navigate({
			moduleName: 'modules/login/login',
			animated: true
		});
	}
	OnTapRegister(args)
	{
		try
		{
			if( this.model.user.name.trim().length <= 0 )
			{
				throw {error: L('Invalid name')};
			}
			if( this.model.user.email.trim().length <= 0 )
			{
				throw {error: L('Invalid username')};
			}
			if( this.model.user.password.trim().length <= 0 )
			{
				throw {error: L('Invalid password')};
			}
			if( this.model.user.rpassword.trim().length <= 0 )
			{
				throw {error: L('Invalid password confirmation')};
			}
			if( this.model.user.password.trim() != this.model.user.rpassword.trim() )
			{
				throw {error: L('The passwords does not match, please review your password')};
			}
			
		}
		catch(e)
		{
			alert({
				title: L('Register Error'),
				message: e.error,
				okButtonText: L('Close')
			});
			return false;
		}
		
		let data 		= this.model.user;
		data.username 	= data.email;
		data.meta 		= {_phone: data.phone};
		spokaneLoading.show({message: L('Processing...')});
		apiWp.Register(data)
			.then( (r) =>
			{
				spokaneLoading.hide();
				let res = r.content.toJSON();
				
				if( parseInt(r.statusCode) != 200 )
					throw res;
					
				alert({
					title: L('Register Success'),
					message: L('You account has been registered, start session with your username and password'),
					okButtonText: L('Close')
				})
				.then( () =>
				{
					this.page.frame.navigate('modules/login/login');
				});
			})
			.catch( (e) =>
			{
				spokaneLoading.hide();
				alert({
					title: L('Register Error'),
					message: e.message || L('Unable to register, please try it later'),
					okButtonText: L('Close')
				});
			});
	}
}
let ctrl 			= new RegisterViewModel();
exports.OnLoaded 	= (args) => ctrl.OnLoaded(args);
