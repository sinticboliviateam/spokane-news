let frame 				= require('ui/frame');
let observableModule 	= require('data/observable');
//let	api					= require('~/shared/api').api;
let ListingViewModel	= require('~/shared/listing').ListingViewModel;

class MediaReleaseViewModel extends ListingViewModel
{
	constructor()
	{
		super();
		//this.apiType	= 'apiWp';
		//this.endpoint 	= '/posts';
		//this.limit = 150;
		this.singleViewModule = 'modules/latestnews/single';
	}
	OnLoaded(args)
	{
		super.OnLoaded(args);
	}
}
let ctrl = new MediaReleaseViewModel();
exports.OnLoaded = (args) => ctrl.OnLoaded(args);
