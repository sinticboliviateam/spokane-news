require('globals');
let observableModule 	= require('tns-core-modules/data/observable');
let applicationSettings	= require('tns-core-modules/application-settings');
let utilityModule 		= require("utils/utils");
let purchase			= require('nativescript-purchase');
let purchaseProduct		= require('nativescript-purchase/product');
let Transaction			= require('nativescript-purchase/transaction').Transaction;
let TransactionState	= require('nativescript-purchase/transaction').TransactionState;
let	api					= require('~/shared/api').api;

class MembershipViewModel
{
	constructor()
	{
		this.page = null;
		this.hasMembership = spokaneUser.user_type == 'paid' ? true : false;
		this.model = null;
		
	}
	OnLoaded(args)
	{
		this.page = args.object;
		this.model = observableModule.fromObject({
			title: L('SPOKANE NEWS PREMIUM VERSION'),
			description: this.GetDescription(),
			payment_aclaration: this.GetPaymentAclaration(),
			thanks_you_message: this.GetThankYouMessage(),
			has_membership: this.hasMembership,
			OnTapBack: (args) => this.OnTapBack(args),
			OnTapSubscription: (args) => this.OnTapSubscription(args),
			OnTapTermsOfUse: (args) => this.OnTapTermsOfUse(args),
			OnTapPrivacyPolicy: (args) => this.OnTapPrivacyPolicy(args)
		});
		this.page.bindingContext = this.model;
		
		try
		{
			
			purchase.on(purchase.transactionUpdatedEvent, (transaction) =>
			{
				if (transaction.transactionState === TransactionState.Purchased)
				{
					alert(`Congratulations you just bought ${transaction.productIdentifier}!`);
					console.log(transaction.transactionDate);
					console.log(transaction.transactionIdentifier);
					applicationSettings.setBoolean(transaction.productIdentifier, true);
				}
				else if (transaction.transactionState === TransactionState.Restored)
				{
					console.log(`Purchase of ${transaction.productIdentifier} restored.`);
					console.log(transaction.transactionDate);
					console.log(transaction.transactionIdentifier);
					console.log(transaction.originalTransaction.transactionDate);
					applicationSettings.setBoolean(transaction.productIdentifier, true);
				}
				else if (transaction.transactionState === TransactionState.Failed)
				{
					//alert(`Purchase of ${transaction.productIdentifier} failed!`);
				}    
			});
			
		}
		catch(e)
		{
			console.dir(e);
		}
		
	}
	GetDescription()
	{
		let txt = 'Premium users enjoy the ability to post their own posting in Buy & Sell, Stolen Vehicles, Missing People '+
					'and Lost & Found Pet Section. Our Premium users get all the benefits of our app including having their '+
					"posting on our app and Spokane News website.\n\n"+
					"Users are not required to subscribe yearly to use our app. The basic app features are free to all users.\n\n" +
					"Any ununser portion of a free trial period, if offered will be forteited when the user purchases a subscription to our publication, where applicable.";

		return txt;
	}
	GetPaymentAclaration()
	{
		let txt = "Payment will be charged to google Account at confirmation of purchase. "+
					"Subscription renew within 24 hours before subscription period end.\n\n" +
					"You can manage your subcription in your google account";
		return txt;
	}
	GetThankYouMessage()
	{
		let txt = "Thank you!\nYou are now a Premium Member of Spokane News\nShare us with everyone.";
		return txt;
	}
	OnTapBack(args)
	{
		this.page.frame.goBack();
	}
	OnTapSubscription(args)
	{
		if ( purchase.canMakePayments() )
		{
			// NOTE: 'product' must be the same instance as the one returned from getProducts()
			purchase.buyProduct(spokaneProducts['subscription_5_usd']);
		}
		else
		{
			alert("Sorry, your account is not eligible to make payments!");
		}
	}
	OnTapTermsOfUse(args)
	{
		utilityModule.openUrl('https://www.spokane-news.com/terms-of-service/');
	}
	OnTapPrivacyPolicy(args)
	{
		utilityModule.openUrl('https://www.spokane-news.com/privacy-policy/');
	}
}
let ctrl = new MembershipViewModel();
exports.OnLoaded = (args) => ctrl.OnLoaded(args);
