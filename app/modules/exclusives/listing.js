let ListingViewModel = require('~/shared/listing').ListingViewModel;

class ExclusivesViewModel extends ListingViewModel
{
	constructor()
	{
		super();
		//this.endpoint = '/app_exclusive.php';
		this.singleViewModule = 'modules/latestnews/single';
		this.itemsArgs = {
			categories: 91
		};
	}	
}
let ctrl = new ExclusivesViewModel();
exports.OnLoaded = (args) => ctrl.OnLoaded(args);
