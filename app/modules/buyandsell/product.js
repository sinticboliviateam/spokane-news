const observableModule 	= require('tns-core-modules/data/observable');
const SocialShare 		= require("nativescript-social-share");
const ImageSourceModule = require("tns-core-modules/image-source");
const http				= require('http');

class ProductViewModel
{
	constructor()
	{
		this.page	= null;
		this.model 	= null;
	}
	OnLoaded(args)
	{
		this.page = args.object;
		this.model = observableModule.fromObject({
			product: observableModule.fromObject({}),
			OnTapBack: (args) => this.OnTapBack(args),
			OnTapContactSeller: (args) => this.OnTapContactSeller(args),
			OnTapBtnShare: (args) => this.OnTapBtnShare(args)
		});
		this.page.bindingContext = this.model;
		this.model.product = this.page.navigationContext.post;
		//sb_admob_banner();
		sb_admob_interstitial();
	}
	OnTapBack(args)
	{
		this.page.frame.navigate({
			moduleName: 'modules/home/home',
			context: {tab: 'buy_sell'},
			animated: true
		});
	}
	OnTapContactSeller(args)
	{
		this.page.frame.navigate({
			moduleName: 'modules/buyandsell/contact-seller',
			context: {product: this.model.product},
			animated: true
		});
	}
	OnTapBtnShare(args)
	{
		sb_show_progress(L('Loading data...'));
		http.getImage(this.model.product.images[0].src).then( (imgSource) =>
		{
			sb_hide_progress();
			SocialShare.shareImage(imgSource, this.model.product.name);
		});
	}
}
let ctrl = new ProductViewModel();
exports.OnLoaded = (args) => ctrl.OnLoaded(args);
