const	observableModule	= require('tns-core-modules/data/observable');

class ContactSellerViewModel
{
	constructor()
	{
		this.page		= null;
		this.model		= observableModule.fromObject({
			product: {},
			data: observableModule.fromObject({
				message: ''
			}),
			OnTapBack: (args) => this.OnTapBack(args),
			OnSubmit: (args) => this.OnSubmit(args)
		});
	}
	OnNavigatingTo(args)
	{
		this.page 			= args.object;
		this.model.product 	= this.page.navigationContext.product;
	}
	OnLoaded(args)
	{
		this.page = args.object;
		this.page.bindingContext = this.model;
		this.model.data.message = L('Hello') + "\n" +
									"I'am interested on your product \""+ this.model.product.name +"\"\n\n" +
									this.model.product.permalink + "\n\n" +
									"Can you give me more information about it?\n\n"+
									"Regards,";
	}
	OnTapBack(args)
	{
		this.page.frame.navigate({
			moduleName: 'modules/buyandsell/product',
			context: {post: this.model.product},
			animated: true
		});
	}
	OnSubmit(args)
	{
		if( this.model.data.message.trim().length <= 0 )
		{
			alert({title: L('Contact seller error'), message: L('You need to write a message for seller'), okButtonText: L('Close')});
			return false;
		}
		
		try
		{
			sb_show_progress(L('Sending your message...'));
			apiWp.post('/contact-seller', {message: this.model.data.message})
				.then( (r) =>
				{
					sb_hide_progress();
					if( r.statusCode != 200 )
						throw r.content.toJSON();
					alert({title: L('Contact seller'), message: L('Your message has been sent to seller.'), okButtonText: L('Ok')})
						.then( () =>
						{
							this.OnTapBack();
						});
				},
				(e) =>
				{
					console.log(e);	
				})
				.catch( (e) =>
				{
					sb_hide_progress();
					alert({title: L('Contact seller error'),
						message: L('An error ocurred while trying to send your message to seller, please try it later.'),
						okButtonText: L('Close')
					});
				});
		}
		catch(e)
		{
			console.log('ERROR_2');
			console.log(e);
			
		}
	}
}
let ctrl = new ContactSellerViewModel();
exports.OnNavigatingTo = (args) => ctrl.OnNavigatingTo(args);
exports.OnLoaded = (args) => ctrl.OnLoaded(args);
