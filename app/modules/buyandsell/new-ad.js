let observableModule		= require('tns-core-modules/data/observable');
let	api						= require('~/shared/api').api;
let imagepicker				= require('nativescript-imagepicker');
let wcApi					= require('~/shared/api-woocommerce').GetInstance();
const ImageSource			= require('tns-core-modules/image-source');


class BuyAndSellNewAdViewModel
{
	constructor()
	{
		this.page = null;
		this.model = observableModule.fromObject({
			ad: observableModule.fromObject({
				title: '',
				description: '',
				location: '',
				price: '',
				image: '~/images/no-image.png',
				status: 'pending'
			}),
			OnTapBack: (args) => this.OnTapBack(args),
			OnTapImage: (args) => this.OnTapImage(args),
			OnSubmitAd: (args) => this.OnSubmitAd(args)
		});
		this.productId = null;
	}
	OnNavigatingTo(args)
	{
		this.productId = null;
		this.page = args.object;
		if( this.page.navigationContext && this.page.navigationContext.id )
		{
			this.productId = this.page.navigationContext.id;
			this.GetProduct();
		}
	}
	OnLoaded(args)
	{
		sb_admob_interstitial();
		this.page 		= args.object;
		this.imageNode 	= this.page.getViewById('ad-image');
		this.page.bindingContext = this.model;
		/*
		wcApi.GetProducts().then( (res) =>
		{
			console.log(res);
		});
		*/
	}
	OnTapBack(args)
	{
		//this.page.frame.goBack();
		this.page.frame.navigate({
			moduleName: 'modules/home/home',
			context: {tab: 'buy_sell'},
			animated: true
		});
	}
	OnTapImage(args)
	{
		let ops = {
			title: L('Image Selection'),
			message: L('Select an image for your ad'),
			cancelButtonText: L('Cancel'),
			actions: [L('Take a picture from camera'), L('Select image from gallery')]
		};
		action(ops).then( (result) =>
		{
			if( L('Select image from gallery') == result )
			{
				let context = imagepicker.create({mode: 'single'});
				context.authorize().then( () =>
				{
					return context.present();
				})
				.then( (selection) =>
				{
					//console.log(JSON.stringify(selection));
					if( selection.length >= 0 )
					{
						this.model.ad.image = selection[0];
						//console.log(this.model.ad.image);
						//console.log(ImageSource.fromFile(selection[0]._android || selection[0]._ios).toBase64String('png'));
						//this.imageNode.src = ImageSource.fromAsset(selection[0]);
						/*
						selection.forEach( (selected) =>
						{
							console.log(selected);
						});
						*/
					}
				});
			}
		});
	}
	OnSubmitAd(args)
	{
		if( this.model.ad.title.length <= 0 )
		{
			alert({title: L('Ad Error'), message: L('You need to enter a title')});
			return false;
		}
		if( this.model.ad.description.length <= 0 )
		{
			alert({title: L('Ad Error'), message: L('You need to enter a description'), okButtonText: L('Close')});
			return false;
		}
		if( this.model.ad.price.length <= 0 )
		{
			alert({title: L('Ad Error'), message: L('You need to enter a price'), okButtonText: L('Close')});
			return false;
		}
		if( isNaN(parseFloat(this.model.ad.price)) )
		{
			alert({title: L('Ad Error'), message: L('You need to enter a valid price'), okButtonText: L('Close')});
			return false;
		}
		
		
		let product = {
			'name': 				this.model.ad.title,
			'type': 				'simple',
			'regular_price': 		this.model.ad.price,
			'description': 			this.model.ad.description,
			'short_description': 	'',
			'status':				'pending',
			'meta_data':			[{key: '_location', value: this.model.ad.location}],
			'categories': 			[],
			'images': 				[],
			'image':				null,
			status:					this.model.ad.status,
			author:					spokaneUser.id
		};
		
		try
		{

			spokaneLoading.show({message: L('Processing...')});
			let promise = null;
			if( this.imageNode.src._android || this.imageNode.src._ios )
			{
				let filename = this.imageNode.src._android || this.imageNode.src._ios;
				let extension	= filename.substr(filename.lastIndexOf('.') + 1);
				product.image = {
					filename: filename,
					imageBase64: ImageSource.fromFile(filename).toBase64String(extension),
				};
			}
			if( !this.productId )
			{
				wcApi.CreateProduct(product).then( (response) =>
				{
					spokaneLoading.hide();
					//console.log(response);
					alert({title: L('Ad Created'), message: L('Your ad has been created'), okButtonText: L('Close')})
						.then( () =>
						{
							this.page.frame.navigate({
								moduleName: 'modules/home/home',
								context: {tab: 'buy_sell'},
								animated: true
							});
						});
				})
				.catch( (e) =>
				{
					spokaneLoading.hide();
					console.log('CREATE PRODUCT ERROR');
				});
			}
			else
			{
				product.id = this.productId;
				wcApi.UpdateProduct(product).then( (response) =>
				{
					spokaneLoading.hide();
					//console.log(response);
					alert({title: L('Ad Updated'), message: L('Your ad has been updated'), okButtonText: L('Close')})
						.then( () =>
						{
							this.page.frame.goBack();
						});
				})
				.catch( (e) =>
				{
					spokaneLoading.hide();
					console.log('UPDATE PRODUCT ERROR');
				});
			}
		}
		catch(e)
		{
			console.log('ERROR');
			console.dir(e);
		}
	}
	GetProduct()
	{
		spokaneLoading.show({message: L('Getting product data...')});
		apiWc.ReadProduct(this.productId)
			.then( (r) =>
			{
				spokaneLoading.hide();
				if( r.statusCode != 200 )
					throw r.content.toJSON();
				this.SetProduct(r.content.toJSON());
			})
			.catch( (e) =>
			{
				spokaneLoading.hide();
				alert({title: L('Error'), message: e.message || e.error || L('Error getting product data')});
			});
	}
	SetProduct(product)
	{
		this.model.ad.title 		= sb_entitydecode(product.name);
		this.model.ad.description 	= sb_striptags(product.description);
		this.model.ad.location 		= '';
		this.model.ad.price 		= product.price;
		this.model.ad.status 		= product.status;
		for(let meta of product.meta_data)
		{
			if( meta.key == '_location' )
				this.model.ad.location = meta.value;
		}
		if( product.images.length > 0 )
		{
			this.model.ad.image = product.images[0].src;
		}
	}
}
let ctrl = new BuyAndSellNewAdViewModel();
exports.OnNavigatingTo = (args) => ctrl.OnNavigatingTo(args);
exports.OnLoaded = (args) => ctrl.OnLoaded(args);
