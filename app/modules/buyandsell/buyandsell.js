let ListingViewModel 	= require('~/shared/listing').ListingViewModel;
//let api					= require('~/shared/api').api;

class BuyAndSellViewModel extends ListingViewModel
{
	constructor()
	{
		super();
		//this.endpoint = '/garage_sales_data.php';
		this.singleViewModule = 'modules/buyandsell/product';
		/*
		this.model.set('activityIndicator', true);
		this.wcApi	= null;
		api.get('/../index.php?mod=spk&task=get_wc_api').then( (response) =>
		{
			let res = JSON.parse(response.content);
			console.log(res);
			this.wcApi = WcApi.GetInstance(res.keys);
		});
		*/
	}
	/*
	OnLoaded(args)
	{
		this.page = args.object;
		this.page.bindingContext = this.model;
		this.GetItems().then( (items) =>
		{
			//console.log('__BUY_SELL_LOADED__');
			this.model.activityIndicator = false;
		});
	}
	*/
}
let ctrl = new BuyAndSellViewModel();
exports.OnNavigatingTo = (args) => ctrl.OnNavigatingTo(args);
exports.OnLoaded = (args) => ctrl.OnLoaded(args);
