let observableModule 	= require('data/observable');
let frameModule			= require('ui/frame');
let api					= require('~/shared/api');

class SinglePromotionViewModel
{
	constructor()
	{
		this.page = null;
		this.model = observableModule.fromObject({
			post: {title: ''},
			comments: [ ],
			OnTapBack: (args) => this.OnTapBack(args)
		});
	}
	OnLoaded(args)
	{
		sb_admob_interstitial();
		this.page = args.object;
		this.page.bindingContext = this.model;
		let post = this.page.navigationContext.post;
		this.model.post = post;
	}
	OnTapBack()
	{
		frameModule.topmost().goBack();
	}
}
let ctrl = new SinglePromotionViewModel();
exports.OnLoaded = (args) => ctrl.OnLoaded(args);
