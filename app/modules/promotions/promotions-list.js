let Listing = require('~/shared/listing').ListingViewModel;
let frame	= require('ui/frame');

class PromotionsViewModel extends Listing
{
	constructor()
	{
		super();
		this.apiType = 'api';
		this.endpoint = '/promotions_posts_data.php';
		this.singleViewModule = 'modules/promotions/single';
	}
	OnTapListItem(args)
	{
		let item = this.model.items[args.index];
		if( !item )
			return false;
		
		frame.topmost().navigate({
			moduleName: this.singleViewModule,
			context: 
			{
				post: {
					post_id:	item.post_id,
					title: 		item.title,
					message: 	item.description,
					picture: 	item.image
				}
			},
			animated: true
		});
	}
}
let ctrl = new PromotionsViewModel();
exports.OnLoaded = (args) => ctrl.OnLoaded(args);
