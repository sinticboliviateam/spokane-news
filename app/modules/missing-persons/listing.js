let ListingViewModel = require('~/shared/listing').ListingViewModel;
//let	api					= require('~/shared/api').api;

class MissingPersonViewModel extends ListingViewModel
{
	constructor()
	{
		super();
		//api.contentType = 'json';
		//this.endpoint = '/../wp-json/wp/v2/missing_person/';
		this.singleViewModule = 'modules/missing-persons/person';
	}
	/*
	OnLoaded(args)
	{
		this.page = args.object;
		this.page.bindingContext = this.model;
		this.GetItems().then( (items) =>
		{
			this.model.activityIndicator = false;
		});
	}
	GetItems()
	{
		return new Promise( (resolve, reject) =>
		{
			api.get(this.endpoint).then( (response) =>
			{
				let res = response.content.toJSON();
				this.model.items = res;
				resolve(res);
			},
			(e) =>
			{
				console.dir(e);
				reject(e);
			});
		});
	}
	*/
}
let ctrl = new MissingPersonViewModel();
exports.OnLoaded = (args) => ctrl.OnLoaded(args);
