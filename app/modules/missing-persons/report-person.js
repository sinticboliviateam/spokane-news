const observableModule		= require('tns-core-modules/data/observable');
const ImageSource			= require('tns-core-modules/image-source');
const imagepicker			= require('nativescript-imagepicker');
const ModalPicker			= require('nativescript-modal-datetimepicker').ModalDatetimepicker;

class ReportPersonViewModel
{
	constructor()
	{
		console.log('ReportPersonViewModel -> constructor');
		this.page = null;
		this.model = null;
		this.imageNode = null;
		this.personId	= null;
	}
	OnNavigatingTo(args)
	{
		this.page = args.object;
		this.model = observableModule.fromObject({
			person: observableModule.fromObject({
				image: '~/images/nobody.png',
				name: '',
				age: '',
				gender: '',
				incident_details: '',
				police_report_number: '',
				missing_date: '',
				contact_number: '',
				status: 'pending'
			}),
			genders: [L('Male'), L('Female')],
			genderIndex: '',
			OnTapBack: (args) => this.OnTapBack(args),
			OnTapPersonImage: (args) => this.OnTapPersonImage(args),
			OnFocusMissingDate: (args) => this.OnFocusMissingDate(args),
			OnTapReportPersonNow: (args) => this.OnTapReportPersonNow(args),
			OnGenderChanged: (args) => this.OnGenderChanged(args)
		});
		console.log(this.page.navigationContext);
		if( this.page.navigationContext && this.page.navigationContext.id )
		{
			this.personId = this.page.navigationContext.id;
			this.GetPerson(this.page.navigationContext.id);
		}
		
	}
	OnLoaded(args)
	{
		sb_admob_interstitial();
		this.page = args.object;
		this.imageNode = this.page.getViewById('person-image');
		
		this.page.bindingContext = this.model;
	}
	OnTapBack(args)
	{
		this.page.frame.goBack();
	}
	OnTapPersonImage(args)
	{
		let ops = {
			title: L('Image Selection'),
			message: L('Select an image for missing person'),
			cancelButtonText: L('Cancel'),
			actions: [L('Take a picture from camera'), L('Select image from gallery')]
		};
		action(ops).then( (result) =>
		{
			if( L('Select image from gallery') == result )
			{
				let context = imagepicker.create({mode: 'single'});
				context.authorize().then( () =>
				{
					return context.present();
				})
				.then( (selection) =>
				{
					if( selection.length >= 0 )
					{
						this.model.person.image = selection[0];
					}
				});
			}
		});
	}
	OnFocusMissingDate(args)
	{
		let picker = new ModalPicker();
		picker.pickDate({
			title: L('Select Date'),
			theme: 'light'
			
		}).then( (result) =>
		{
			let date = new Date(result.year, result.month - 1, result.day);
			this.model.person.missing_date = sb_formatdate(date, 'Y-m-d');
		})
		.catch( (e) =>
		{
			
		});
	}
	OnGenderChanged(args)
	{
		this.model.person.gender = this.model.genders[args.newIndex] || '';
	}
	OnTapReportPersonNow(args)
	{
		if( this.model.person.name.trim().length <= 0 )
		{
			alert({title: L('Person Name Error'), message: L('You need to enter a person name'), okButtonText: L('Close')});
			return false;
		}
		if( this.model.person.incident_details.trim().length <= 0 )
		{
			alert({title: L('Encident Error'), message: L('You need to describe the incident'), okButtonText: L('Close')});
			return false;
		}
		if( this.model.person.age.trim().length <= 0 || isNaN(parseInt(this.model.person.age.trim())) )
		{
			alert({title: L('Age Error'), message: L('You need to enter a person age'), okButtonText: L('Close')});
			return false;
		}
		if( this.model.person.missing_date.trim().length <= 0 )
		{
			alert({title: L('Mising Date Error'), message: L('You need to enter missing date'), okButtonText: L('Close')});
			return false;
		}
		if( this.model.person.gender.trim().length <= 0 )
		{
			alert({title: L('Gender Error'), message: L('You need to select the person gender'), okButtonText: L('Close')});
			return false;
		}
		if( this.model.person.contact_number.trim().length <= 0 )
		{
			alert({title: L('Contact Number Error'), message: L('You need to enter a contact number'), okButtonText: L('Close')});
			return false;
		}
		let person = {
			status:				!this.personId ? 'pending' : this.model.person.status,
			title:				this.model.person.name,
			content:			this.model.person.incident_details,
			//featured_media:		0,
			person_data:		{
				'_age':				this.model.person.age,
				'_missing_date':	this.model.person.missing_date,
				'_gender':			this.model.person.gender,
				'_police_report':	this.model.person.police_report_number,
				'_contact_number':	this.model.person.contact_number
			},
			image: null
		};
		//person.meta = person.person_data;
		if( this.imageNode.src._android || this.imageNode.src._ios )
		{
			let filename 	= this.imageNode.src._android || this.imageNode.src._ios;
			let extension	= filename.substr(filename.lastIndexOf('.') + 1);
			person.image = {
				filename: 		filename,
				imageBase64: 	ImageSource.fromFile(filename).toBase64String(extension),
			};
		}
		
		spokaneLoading.show({message: L('Processing...')});
		if( this.personId )
		{
			person.id = this.personId;
			apiWp.UpdateMissingPerson(person)
				.then( (response) =>
				{
					spokaneLoading.hide();
					let res = response.content.toJSON();
					console.log(`StatusCode: ${response.statusCode}`);	
					if( response.statusCode != 200 && response.statusCode != 201 )
						throw res;
						
					alert({
						title: L('Update Missing Person Success'),
						message: L('The missing person has been updated.'),
						okButtonText: L('Close')
					}).then( () =>
					{
						this.page.frame.goBack();
					});
				})
				.catch(	(e) =>
				{
					spokaneLoading.hide();
					console.log(e);
					alert({
						title: L('Report Missing Person Error'),
						message: L('An error ocurred while trying to report the missing person, please try it later'),
						okButtonText: L('Close')
					});
				});
		}
		else
		{
			apiWp.CreateMissingPerson(person)
				.then( (response) =>
				{
					spokaneLoading.hide();
					let res = response.content.toJSON();
					
					if( response.statusCode != 200 && response.statusCode != 201 )
						throw res;
						
					alert({
						title: L('Report Missing Person Success'),
						message: L('The missing person has been reported.'),
						okButtonText: L('Close')
					}).then( () =>
					{
						this.page.frame.goBack();
					});
				})
				.catch(	(e) =>
				{
					spokaneLoading.hide();
					console.log(e);
					alert({
						title: L('Report Missing Person Error'),
						message: L('An error ocurred while trying to report the missing person, please try it later'),
						okButtonText: L('Close')
					});
				});
		}
	}
	GetPerson(personId)
	{
		spokaneLoading.show({message: L('Retrieving person data')});
		apiWp.ReadCustomPost(personId, 'missing_person')
			.then( (r) =>
			{
				spokaneLoading.hide();
				if( r.statusCode != 200 )
					throw r.content.toJSON();
				let person = r.content.toJSON();
				this.SetPersonData(person);
			})
			.catch( (e) =>
			{
				spokaneLoading.hide();
				console.log('ERROR GETTING WP PERSON');
				console.log(e);
			});
	}
	SetPersonData(person)
	{
		try
		{
			if( person.featured_image )
				this.model.person.image = person.featured_image;
			this.model.person.name 		= sb_entitydecode(person.title.rendered);
			this.model.person.age 		= person.person_data._age;
			this.model.person.gender 	= person.person_data._gender;
			this.model.genderIndex		= person.person_data._gender ?
											this.model.genders.indexOf(person.person_data._gender) : '';
			this.model.person.incident_details = sb_striptags(person.content.rendered);
			this.model.person.police_report_number = person.person_data._police_report;
			this.model.person.missing_date = sb_formatdate(person.person_data._missing_date, 'Y-m-d');
			this.model.person.contact_number = person.person_data._contact_number;
			this.model.person.status			= person.status;
		}
		catch(e)
		{
			console.log('ERROR SETTING PERSON DATA');
			console.log(e);
		}
		
	}
}
let ctrl = new ReportPersonViewModel();
exports.OnNavigatingTo 	= (args) => ctrl.OnNavigatingTo(args);
exports.OnLoaded 		= (args) => ctrl.OnLoaded(args);
