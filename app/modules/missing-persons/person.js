const observableModule		= require('tns-core-modules/data/observable');
const SBCommentsModule		= require('~/shared/comment/sb-comment');

class PersonViewModel
{
	constructor()
	{
		this.page = null;
		this.model = null;
	}
	OnNavigatingTo(args)
	{
		this.model = observableModule.fromObject({
			person: observableModule.fromObject({
				title: '',
				content: ''
			}),
			comment: '',
			OnTapBack: (args) => this.OnTapBack(args),
			OnTapButtonSendComment: (args) => this.OnTapButtonSendComment(args)
		});
		sb_admob_interstitial();
	}
	OnLoaded(args)
	{
		this.page = args.object;
		this.stackLayoutComments = this.page.getViewById('listing-comments');
		this.page.bindingContext = this.model;
		let person = this.page.navigationContext.post;
		//console.dir(person);
		this.model.person = person;
		//this.GetPerson(person.id);
		this.RefreshComments();
	}
	OnTapBack(args)
	{
		//this.page.frame.goBack();
		this.page.frame.navigate({
			moduleName: 'modules/home/home',
			context:{tab: 'missing_persons'},
			animated: true
		});
	}
	GetPerson()
	{
	}
	RefreshComments()
	{
		apiWp.ReadAllComments(this.model.person.id, 1, 100)
			.then( (r) =>
			{
				let res = r.content.toJSON();
				this.FillComments(res);
			})
			.catch( (e) =>
			{
			});
	}
	OnTapButtonSendComment(args)
	{
		spokaneLoading.show({message: L('Please wait...')});
		//console.log(`COMMENT => ${this.model.comment}`);
		apiWp.CreateComment(this.model.person.id, this.model.comment)
			.then( (response) =>
			{
				this.model.comment = '';
				spokaneLoading.hide();
				let res = response.content.toJSON();
				if( response.statusCode != 201 )
				{
					throw res;
				}
				this.RefreshComments();
			})
			.catch( (e) =>
			{
				spokaneLoading.hide();
				alert({title: 'ERROR', message: e.message || L('An error ocurred while trying to send your comment, please try again later.')});
			});
	}
	FillComments(items)
	{
		for(let item of items)
		{
			try
			{
				let c = new SBCommentsModule.SBComment();
				c.className = 'white';
				c.SetComment({
					author: {
						image: item.author_avatar_urls['48'],
						name: item.author_name
					},
					content: sb_striptags(item.content.rendered.trim()),
					date_time: item.date
				});
				this.stackLayoutComments.addChild(c);
			}
			catch(e)
			{
				console.dir(e);
				console.log('ERROR FILLING COMMENTS');
			}
		}
	}
}
let ctrl = new PersonViewModel();
exports.OnNavigatingTo = (args) => ctrl.OnNavigatingTo(args);
exports.OnLoaded = (args) => ctrl.OnLoaded(args);
