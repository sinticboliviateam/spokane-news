//let frame					= require('ui/frame');
let observableModule		= require('tns-core-modules/data/observable');
//let	api						= require('~/shared/api').api;
let ApplicationSettings		= require('application-settings');
let LoadingIndicator		= require('nativescript-loading-indicator').LoadingIndicator;
let imagepicker				= require('nativescript-imagepicker');
const ImageSource			= require('tns-core-modules/image-source');

class ProfileViewModel
{
	constructor()
	{
		this.page 			= null;
		//this.currentUser 	= JSON.parse(ApplicationSettings.getString('user'));
		//console.dir(this.currentUser);
		this.model			= null;
		this.avatar			= null;
		this.loader			= new LoadingIndicator();
		this.model 	= observableModule.fromObject({
			user: observableModule.fromObject({
				name: '',
				email: '',
				password: '',
				phone: '',
				zipcode: '',
				location: '',
				picture: '',
			}),
			OnTapBack: (args) => this.OnTapBack(args),
			OnTapAvatar: (args) => this.OnTapAvatar(args),
			OnTapSave: (args) => this.OnTapSave(args)
		});
	}
	OnNavigatingTo(args)
	{
		this.GetUserData().then( (userData) =>
		{
			this.SetUserData(userData);
		});
	}
	OnLoaded(args)
	{
		this.page 	= args.object;
		
		this.page.bindingContext = this.model;
		this.avatar = this.page.getViewById('user-avatar');
		
	}
	OnTapBack(args)
	{
		//frame.topmost().goBack();
		//args.object.page.frame.goBack();
		args.object.page.frame.navigate({
			moduleName: 'modules/home/home',
			animate: true
		});
	}
	OnTapAvatar(args)
	{
		let ops = {
			title: L('Image Selection'),
			message: L('Select an option for you avatar'),
			cancelButtonText: L('Cancel'),
			actions: [L('Take a picture from camera'), L('Select image from gallery')]
		};
		action(ops).then( (result) =>
		{
			if( L('Select image from gallery') == result )
			{
				let context = imagepicker.create({mode: 'single'});
				context.authorize().then( () =>
				{
					return context.present();
				})
				.then( (selection) =>
				{
					//selection.forEach( (selected) =>
					for(let selected of selection)
					{
						console.log(selected);
						this.model.user.picture = selected;
					}
				});
			}
		});
	}
	OnTapSave(args)
	{
		let parts = this.model.user.name.trim().split(' ');
		let firstname = '';
		let lastname = '';
		if( parts.length == 4 )
		{
			firstname = `${parts[0]} ${parts[1]}`;
			lastname = `${parts[2]} ${parts[3]}`;
		}
		else if( parts.length == 3 )
		{
			firstname = `${parts[0]}`;
			lastname = `${parts[1]} ${parts[2]}`;
		}
		else if( parts.length == 2 )
		{
			firstname = `${parts[0]}`;
			lastname = `${parts[1]}`;
		}
		else
		{
			firstname = `${parts[0]}`;
		}
		let userData = {
			first_name: firstname,
			last_name: lastname,
			name: `${firstname} ${lastname}`,
			email: this.model.user.email,
			meta_data: {
				_phone: this.model.user.phone,
				_zip: this.model.user.zipcode,
				_location: this.model.user.location
			}
		};
		//##try to save new avatar
		if( this.avatar.src._android || this.avatar.src._ios )
		{
			let filename 	= this.avatar.src._android || this.avatar.src._ios;
			let extension	= filename.substr(filename.lastIndexOf('.') + 1);
			userData.avatar_image = {
				filename: filename,
				imageBase64: ImageSource.fromFile(filename).toBase64String(extension),
			};
			
		}
		this.loader.show({message: L('Updating your profile...')});
		apiWp.post('/users/me', userData).then((r) =>
		{
			this.loader.hide();
			//##update current user
			let usr = r.content.toJSON();
			//console.log(usr.meta_data);
			spokaneUser.user_display_name 	= usr.name;
			spokaneUser.user_email 			= usr.email;
			spokaneUser.picture				= usr.meta_data._picture;
			ApplicationSettings.setString('user', JSON.stringify(spokaneUser));
			
			alert({title: L('Profile Updated'), message: L('Your profile has been updated'), okButtonText: L('Close')});
		},
		(e) =>
		{
			alert({title: L('Error Updating Profile'), message: L('Try it later'), okButtonText: L('Close')});
		});
		
		/*
		let params = `id=${this.currentUser.id}`+
						`&name=${this.model.user.name}` +
						`&phone=${this.model.user.phone}` +
						`&password=${this.model.user.password}`+
						`&zip=${this.model.user.zipcode}` +
						`&location=${this.model.user.location}`;
		//console.log(params);
		this.loader.show({message: L('Updating your profile...')});
		api.post('/edit_profile.php', params).then( (response) =>
		{
			this.loader.hide();
			let res = JSON.parse(response.content);
			alert({title: L('Profile Updated'), message: res.api_data});
		});
		*/
	}
	GetUserData()
	{
		this.loader.show({message: L('Please wait, retrieving your info...')});
		return new Promise( (resolve, reject) =>
		{
			apiWp.get('/users/me').then( (response) =>
			{
				//console.log(response);
				this.loader.hide();
				resolve(response.content.toJSON());
			});
		});
	}
	SetUserData(data)
	{
		console.dir(data);
		this.model.user.name 		= data.name;
		this.model.user.email 		= data.email_address;
		this.model.user.phone		= data.meta_data._phone;
		this.model.user.zipcode		= data.meta_data._zip;
		this.model.user.location	= data.meta_data._location;
		if( !this.model.user.picture )
			this.model.user.picture		= data.meta_data._picture;
	}
}
let ctrl = new ProfileViewModel();
exports.OnNavigatingTo = (args) => ctrl.OnNavigatingTo(args);
exports.OnLoaded = (args) => ctrl.OnLoaded(args);
