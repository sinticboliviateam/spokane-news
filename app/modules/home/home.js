const   builder             = require('ui/builder');
let     applicationSettings = require('application-settings');
let     StackLayout         = require('ui/layouts/stack-layout').StackLayout;
let     GridLayout          = require('ui/layouts/grid-layout').GridLayout;
let     AbsoluteLayout      = require('ui/layouts/absolute-layout').AbsoluteLayout;
let     Label               = require('ui/label').Label;
let     Button              = require('ui/button').Button;
let     Image               = require('ui/image').Image;
let     platform            = require('platform');
let     gestures            = require("ui/gestures");
//let     application         = require('application');
let     observableModule    = require('data/observable');
let     frameModule         = require('ui/frame');
let     LocalNotifications  = require('nativescript-local-notifications').LocalNotifications;
let		utilsModule			= require('utils/utils');
let 	admob 				= require("nativescript-admob");
class HomeViewModel
{
	constructor()
    {
        this.page       = null;
        this.navigation = null;
        this.model		= null;
		this.nav		= null;
		this.controls	= null;
		this.bannerImage  = null;
		this.homeTabs		= null;
		this.sponsorsScroll	= null;
		this.interval 		= null;
    }
    OnNavigatingTo(args)
    {
		this.page               = args.object;
		setInterval(() =>
        {
			this.bannerImage.animate({ opacity: 0, duration: 1000, iterations: 4 }).then( () =>
			{
				this.bannerImage.opacity = 1;
			});
		}, 30000);
		this.model		= observableModule.fromObject({
			OnSelectedIndexChanged: (args) => this.OnSelectedIndexChanged(args),
			OnTapAdFreeVersion: (args) => this.OnTapAdFreeVersion(args),
			tabIndex: 0
		});
	}
	OnLoaded(args)
    {
		sb_admob_banner();
        this.page			= args.object;
        this.nav 			= this.page.getViewById('navigation');
		this.controls 		= this.page.getViewById('nav-controls');
		this.bannerImage	= this.page.getViewById('banner-image');
		this.homeTabs		= this.page.getViewById('home-tabs');
		this.sponsorsScroll	= this.page.getViewById('sponsors-scroll');
        let tabIndex		= 0;
        let banner			= this.page.getViewById('banner-free-ad');
        
		if( spokaneUser && spokaneUser.user_type != 'free' && banner )
		{
			this.page.getViewById('inner-content').removeChild(banner);
		}
        //##check for navigation context
		if( this.page.navigationContext )
		{
			let context = this.page.navigationContext;
			let tabViewItem = null;
			if( context.tab )
				tabViewItem = this.SetTab(context.tab);
				
			switch(context.action)
			{
				case 'refresh':
					console.log(tabViewItem.getViewById('page-latestnews').bindingContext);
					//tabViewItem.getViewById('page-latestnews').bindingContext.RefreshNews();
				break;
			}
		}
        
        this.page.bindingContext = this.model;
        this.nav.bindingContext.title = L('Latest News');
        this.SetSponsors();
        
    }
    OnPageUnLoaded(args)
    {
	}
	SetTab(key)
	{
		let tabViewItem = null;
		let index = 0;
		for(let item of this.homeTabs.items)
		{
			if( key == item.key )
			{
				this.model.tabIndex = index;
				tabViewItem = item;
				break;
			}
			index++;
		}

		return tabViewItem;
	}
	OnSelectedIndexChanged(args)
	{
		
		let tabViewItem	= args.object.items[args.newIndex];
		
		this.controls.eachChild( (child) =>
		{
			this.controls.removeChild(child);
		});
		this.nav.bindingContext.show_title = true;
		this.nav.bindingContext.title = tabViewItem.title;
		
		if( tabViewItem.key == 'latestnews'  )
		{
			//nav.bindingContext.title = L('Latest News');
		}
		else if( tabViewItem.key == 'media_releases' )
		{
			//nav.bindingContext.title = L('Media Releases');
		}
		else if( tabViewItem.key == 'promotions' )
		{
		}
		else if( tabViewItem.key == 'buy_sell' )
		{
			//nav.bindingContext.show_title = false;
			let btn = new Button();
			btn.text = L('Post New Ad');
			btn.className = 'btn btn-primary btn-xs';
			btn.on(Button.tapEvent, (args) =>
			{
				this.page.frame.navigate('modules/buyandsell/new-ad');
			});
			this.controls.addChild(btn);
		}
		else if( tabViewItem.key == 'app_exclusives' )
		{
		}
		else if( tabViewItem.key == 'missing_persons' )
		{
			let btn = new Button();
			btn.text = L('Post Missing Person');
			btn.className = 'btn btn-primary btn-xs';
			btn.on(Button.tapEvent, (args) =>
			{
				this.page.frame.navigate('modules/missing-persons/report-person');
			});
			this.controls.addChild(btn);
		}
		else if( tabViewItem.key == 'lost_found_pets' )
		{
			
			let btn = new Button();
			btn.text = L('Post Lost/Found Pet');
			btn.className = 'btn btn-primary btn-xs';
			btn.on(Button.tapEvent, (args) =>
			{
				this.page.frame.navigate('modules/lost-pets/report-pet');
			});
			this.controls.addChild(btn);
		}
		
	}
	OnTapAdFreeVersion(args)
	{
		this.page.frame.navigate({
			moduleName: 'modules/membership/membership',
			animate: true
		});
	}
	SetSponsors()
	{
		
		let container = this.page.getViewById('sponsors');
		if( !container )
			return false;
		container.removeChildren();
		let totalSponsors = appSettings.sponsors.length;
		if( totalSponsors <= 0 )
			return false;
			
		for(let spon of appSettings.sponsors)
		{
			container.addChild(this.BuildSponsor(spon));
		}
		//##set ratotion
		this.sponsorsScroll.scrollBarIndicatorVisible = false;
		if( this.interval )
			clearInterval(this.interval);
		let nextIndex = 0;
		let step = 1;
		this.interval = setInterval(() =>
		{
			let totalScroll = this.sponsorsScroll.scrollableWidth;
			let nextScroll = 0;
			if(this.sponsorsScroll.horizontalOffset >= totalScroll)
			{
				container.addChild(this.BuildSponsor(appSettings.sponsors[nextIndex]));
				if( (nextIndex % 2) == 1 )
				{
					container.removeChild(container.getChildAt(nextIndex - 1));
				}
				nextIndex++;
				if( nextIndex >= totalSponsors )
				{
					nextIndex = 0;
				}
				totalScroll = this.sponsorsScroll.scrollableWidth;
			}
			//~ else
			//~ {
				nextScroll = this.sponsorsScroll.horizontalOffset + step;
			//~ }
			this.sponsorsScroll.scrollToHorizontalOffset(nextScroll, true);
		}, 100);
	}
	BuildSponsor(spon)
	{
		let btn 			= new Image();
		btn.src 			= spon.image;
		btn.stretch 		= 'aspectFill';
		btn.automationText 	= spon.name;
		btn.on(Button.tapEvent, (args) =>
		{
			utilsModule.openUrl(spon.url);
		});

		return btn;
	}
}
let ctrl = new HomeViewModel();
exports.OnNavigatingTo  = (args) => ctrl.OnNavigatingTo(args);
exports.OnLoaded        = (args) => ctrl.OnLoaded(args);
exports.OnPageUnloaded  = (args) => ctrl.OnPageUnLoaded(args);
