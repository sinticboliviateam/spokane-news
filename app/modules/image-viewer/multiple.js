const	observableModule 	= require('tns-core-modules/data/observable');
const	ObservableArray		= require('tns-core-modules/data/observable-array').ObservableArray;
const	Image				= require('tns-core-modules/ui/image').Image;
const	gestures 			= require("tns-core-modules/ui/gestures");

class ImageViewerViewModel
{
	constructor()
	{
		this.page = null;
		this.model = observableModule.fromObject({
			images: new ObservableArray([]),
			OnTapCloseModal: (args) => this.OnTapCloseModal(args)
		});
	}
	OnNavigatingTo(args)
	{
		this.page = args.object;
		this.model.images = this.page.navigationContext.images;
	}
	OnLoaded(args)
	{
		this.page = args.object;
		this.page.bindingContext = this.model;
		this.imagesContainer = this.page.getViewById('container-images');
	}
	OnShownModally(args)
	{
		this.page = args.object;
		this.page.bindingContext = this.model;
		console.log(args.context);
		if( args.context.images )
		{
			//this.model.images = new ObservableArray(args.context.images);
			this.model.images = args.context.images;
			/*
			for(let item of this.model.images)
			{
				let img = new Image();
				img.src = item.src;
				img.className = '';
				img.stretch = 'aspectFill';
				img.on(gestures.GestureTypes.pinch, ($args) =>
				{
					console.log(`Pinch scale: ${$args.scale}`);
				});
				this.imagesContainer.addChild(img);
			}
			*/
		}
	}
	OnTapCloseModal(args)
	{
		this.page.closeModal();
	}
}
let ctrl = new ImageViewerViewModel();
exports.OnNavigatingTo	= (args) => ctrl.OnNavigatingTo(args);
exports.OnLoaded		= (args) => ctrl.OnLoaded(args);
exports.OnShownModally	= (args) => ctrl.OnShownModally(args);
