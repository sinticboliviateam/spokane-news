const observableModule		= require('tns-core-modules/data/observable');
const ImageSource			= require('tns-core-modules/image-source');
const imagepicker			= require('nativescript-imagepicker');
const ModalPicker			= require('nativescript-modal-datetimepicker').ModalDatetimepicker;

class ReportPetViewModel
{
	constructor()
	{
		this.page 		= null;
		this.model 		= null;
		this.imageNode 	= null;
		this.petId		= null;
	}
	OnNavigatingTo(args)
	{
		this.petId		= null;
		this.page = args.object;
		this.model = observableModule.fromObject({
			pet: observableModule.fromObject({
				image: '~/images/lost-pet.jpg',
				name: '',
				age: '',
				gender: '',
				incident_details: '',
				police_report_number: '',
				missing_date: '',
				contact_number: '',
				status: 'pending'
			}),
			genders: [L('Male'), L('Female')],
			genderIndex: '',
			OnTapBack: (args) => this.OnTapBack(args),
			OnTapPetImage: (args) => this.OnTapPetImage(args),
			OnFocusMissingDate: (args) => this.OnFocusMissingDate(args),
			OnTapReportPetNow: (args) => this.OnTapReportPetNow(args),
			OnGenderChanged: (args) => this.OnGenderChanged(args)
		});
		if( this.page.navigationContext && this.page.navigationContext.id )
		{
			this.petId = this.page.navigationContext.id;
			this.GetPet();
		}
	}
	OnLoaded(args)
	{
		sb_admob_interstitial();
		this.page = args.object;
		this.imageNode = this.page.getViewById('pet-image');
		this.page.bindingContext = this.model;
	}
	OnTapBack(args)
	{
		this.page.frame.goBack();
	}
	OnTapPetImage(args)
	{
		let ops = {
			title: L('Image Selection'),
			message: L('Select an image for lost pet'),
			cancelButtonText: L('Cancel'),
			actions: [L('Take a picture from camera'), L('Select image from gallery')]
		};
		action(ops).then( (result) =>
		{
			if( L('Select image from gallery') == result )
			{
				let context = imagepicker.create({mode: 'single'});
				context.authorize().then( () =>
				{
					return context.present();
				})
				.then( (selection) =>
				{
					if( selection.length >= 0 )
					{
						this.model.pet.image = selection[0];
					}
				});
			}
		});
	}
	OnGenderChanged(args)
	{
		this.model.pet.gender = this.model.genders[args.newIndex] || '';
	}
	OnFocusMissingDate(args)
	{
		let picker = new ModalPicker();
		picker.pickDate({
			title: L('Select Date'),
			theme: 'light'
			
		}).then( (result) =>
		{
			let date = new Date(result.year, result.month - 1, result.day);
			this.model.pet.missing_date = sb_formatdate(date, 'Y-m-d');
		})
		.catch( (e) =>
		{
			
		});
	}
	OnTapReportPetNow(args)
	{
		if( this.model.pet.name.trim().length <= 0 )
		{
			alert({title: L('Pet Name Error'), message: L('You need to enter a pet name'), okButtonText: L('Close')});
			return false;
		}
		if( this.model.pet.incident_details.trim().length <= 0 )
		{
			alert({title: L('Encident Error'), message: L('You need to describe the incident'), okButtonText: L('Close')});
			return false;
		}
		if( this.model.pet.age.trim().length <= 0 || isNaN(parseInt(this.model.pet.age.trim())) )
		{
			alert({title: L('Age Error'), message: L('You need to enter a pet age'), okButtonText: L('Close')});
			return false;
		}
		if( this.model.pet.missing_date.trim().length <= 0 )
		{
			alert({title: L('Mising Date Error'), message: L('You need to enter missing date'), okButtonText: L('Close')});
			return false;
		}
		if( this.model.pet.gender.trim().length <= 0 )
		{
			alert({title: L('Gender Error'), message: L('You need to select the pet gender'), okButtonText: L('Close')});
			return false;
		}
		if( this.model.pet.contact_number.trim().length <= 0 )
		{
			alert({title: L('Contact Number Error'), message: L('You need to enter a contact number'), okButtonText: L('Close')});
			return false;
		}
		let pet = {
			status:				!this.petId ? 'pending' : this.model.pet.status,
			title:				this.model.pet.name,
			content:			this.model.pet.incident_details,
			//featured_media:		0,
			pet_data:		{
				'_age':				this.model.pet.age,
				'_missing_date':	this.model.pet.missing_date,
				'_gender':			this.model.pet.gender,
				//'_police_report':	this.model.pet.police_report_number,
				'_contact_number':	this.model.pet.contact_number
			},
			image: null
		};
		
		if( this.imageNode.src._android || this.imageNode.src._ios )
		{
			let filename 	= this.imageNode.src._android || this.imageNode.src._ios;
			let extension	= filename.substr(filename.lastIndexOf('.') + 1);
			pet.image = {
				filename: 		filename,
				imageBase64: 	ImageSource.fromFile(filename).toBase64String(extension),
			};
		}
		
		spokaneLoading.show({message: L('Processing...')});
		if( !this.petId )
		{
			apiWp.CreateLostPet(pet)
				.then( (response) =>
				{
					spokaneLoading.hide();
					let res = response.content.toJSON();
					if( response.statusCode != 200 && response.statusCode != 201 )
						throw res;
						
					alert({
						title: L('Report Lost Pet Success'),
						message: L('The lost pet has been reported.'),
						okButtonText: L('Close')
					}).then( () =>
					{
						this.page.frame.goBack();
					});
				})
				.catch(	(e) =>
				{
					spokaneLoading.hide();
					console.log(e);
					alert({
						title: L('Report Lost Pet Error'),
						message: L('An error ocurred while trying to report the lost pet, please try it later'),
						okButtonText: L('Close')
					});
				});
		}
		else
		{
			pet.id = this.petId;
			apiWp.UpdateLostPet(pet)
				.then( (response) =>
				{
					spokaneLoading.hide();
					let res = response.content.toJSON();
					if( response.statusCode != 200 && response.statusCode != 201 )
						throw res;
						
					alert({
						title: L('Update Success'),
						message: L('The lost pet has been updated.'),
						okButtonText: L('Close')
					}).then( () =>
					{
						this.page.frame.goBack();
					});
				})
				.catch(	(e) =>
				{
					spokaneLoading.hide();
					console.log(e);
					alert({
						title: L('Update Lost Pet Error'),
						message: e.message || e.error || L('An error ocurred while trying to report the lost pet, please try it later'),
						okButtonText: L('Close')
					});
				});
		}
	}
	GetPet()
	{
		spokaneLoading.show({message: L('Retrieving pet data')});
		apiWp.ReadCustomPost(this.petId, 'lostpet')
			.then( (r) =>
			{
				spokaneLoading.hide();
				if( r.statusCode != 200 )
					throw r.content.toJSON();
				let pet = r.content.toJSON();
				this.SetPetData(pet);
			})
			.catch( (e) =>
			{
				spokaneLoading.hide();
				console.log('ERROR GETTING WP PET');
				console.log(e);
			});
	}
	SetPetData(pet)
	{
		try
		{
			if( pet.featured_image )
				this.model.pet.image = pet.featured_image;
			this.model.pet.name 		= sb_entitydecode(pet.title.rendered);
			this.model.pet.age 			= pet.pet_data._age;
			this.model.pet.gender 		= pet.pet_data._gender;
			this.model.genderIndex		= pet.pet_data._gender ?
											this.model.genders.indexOf(pet.pet_data._gender) : '';
			this.model.pet.incident_details = sb_striptags(pet.content.rendered);
			//this.model.pet.police_report_number = pet.pet_data._police_report;
			this.model.pet.missing_date = sb_formatdate(pet.pet_data._missing_date, 'Y-m-d');
			this.model.pet.contact_number = pet.pet_data._contact_number;
			this.model.pet.status			= pet.status;
		}
		catch(e)
		{
			console.log('ERROR SETTING PET DATA');
			console.log(e);
		}
	}
}
let ctrl = new ReportPetViewModel();
exports.OnNavigatingTo 	= (args) => ctrl.OnNavigatingTo(args);
exports.OnLoaded 		= (args) => ctrl.OnLoaded(args);
