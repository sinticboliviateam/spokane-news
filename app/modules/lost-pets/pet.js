const observableModule		= require('tns-core-modules/data/observable');
const GridLayoutModule		= require('tns-core-modules/ui/layouts/grid-layout');
const StackLayout			= require('tns-core-modules/ui/layouts/stack-layout').StackLayout;
const Label					= require('tns-core-modules/ui/label').Label;
const Image					= require('tns-core-modules/ui/image').Image;
const SBCommentsModule		= require('~/shared/comment/sb-comment');

class PetViewModel
{
	constructor()
	{
		this.page 				= null;
		this.model 				= null;
		this.listingComments 	= null;
		this.model = observableModule.fromObject({
			pet: observableModule.fromObject({
				title: '',
				content: ''
			}),
			comment: '',
			activityIndicator: true,
			showButtonMore: false,
			OnTapBack: (args) => this.OnTapBack(args),
			OnTapButtonSendComment: (args) => this.OnTapButtonSendComment(args),
			OnTapLoadMoreComments: (args) => this.OnTapLoadMoreComments(args)
		});
		this.commentsPage		= 0;
	}
	OnNavigatingTo(args)
	{
		sb_admob_interstitial();
	}
	OnLoaded(args)
	{
		
		this.page 					= args.object;
		this.listingComments 		= this.page.getViewById('listing-comments');
		this.page.bindingContext 	= this.model;
		let pet 					= this.page.navigationContext.post;
		this.model.pet 				= pet;
		this.GetComments(1)
			.then( (items) =>
			{
				this.FillComments(items);
			})
			.catch( (e) =>
			{
				
			});
	}
	OnTapBack(args)
	{
		//this.page.frame.goBack();
		this.page.frame.navigate({
			moduleName: 'modules/home/home',
			context:{tab: 'lost_found_pets'},
			animated: true
		});
	}
	OnTapButtonSendComment(args)
	{
		spokaneLoading.show({message: L('Please wait...')});
		let comment = args.object.text.trim();
		//console.log(`COMMENT => ${this.model.comment}`);
		apiWp.CreateComment(this.model.pet.id, this.model.comment)
			.then( (response) =>
			{
				this.model.comment = '';
				spokaneLoading.hide();
				let res = response.content.toJSON();
				if( response.statusCode != 201 )
				{
					throw res;
				}
				this.RefreshComments();
			})
			.catch( (e) =>
			{
				spokaneLoading.hide();
				alert({title: 'ERROR', message: e.message || L('An error ocurred while trying to send your comment, please try again later.')});
			});
	}
	GetComments(page, limit)
	{
		page 	= page && parseInt(page) > 1 ? page : 1;
		limit 	= limit || 50;
		
		return new Promise( (resolve, reject) =>
		{
			this.model.activityIndicator = true;
			apiWp.ReadAllComments(this.model.pet.id, this.commentsPage, limit)
				.then( (result) =>
				{
					this.model.activityIndicator = false;
					if( result.statusCode != 200 )
						throw result.content.toJSON();
					let items = result.content.toJSON();
					let totalRows	= parseInt(result.headers['X-WP-Total']);
					let totalPages 	= parseInt(result.headers['X-WP-TotalPages']);
					if( page < totalPages )
					{
						this.commentsPage = page;
						this.model.showButtonMore = true;
					}
					else
					{
						this.model.showButtonMore = false;
					}
					resolve(items);
				})
				.catch( (e) =>
				{
					this.model.activityIndicator = false;
					reject(e);
				});
		});
	}
	RefreshComments()
	{
		this.GetComments(1, 50)
			.then( (items) =>
			{
				this.FillComments(items, true);
			})
			.catch( (e) =>
			{
				
			});
	}
	FillComments(items, clear)
	{
		if( clear )
			this.listingComments.removeChildren();
			
		for(let item of items)
		{
			try
			{
				let c = new SBCommentsModule.SBComment();
				c.SetComment({
					author: {
						image: item.author_picture || item.author_avatar_urls['48'],
						name: item.author_name
					},
					content: sb_n2br(sb_striptags(item.content.rendered.trim())),
					date_time: sb_formatdate(item.date_gmt, 'M d Y H:i:s')
				});
				this.listingComments.addChild(c);
			}
			catch(e)
			{
				console.dir(e);
				console.log('ERROR FILLING COMMENTS');
			}
		}
	}
	OnTapLoadMoreComments(args)
	{
		this.GetComments(this.commentsPage + 1)
			.then( (items) =>
			{
				this.FillComments(items);
			});
	}
}
let ctrl = new PetViewModel();
exports.OnNavigatingTo = (args) => ctrl.OnNavigatingTo(args);
exports.OnLoaded = (args) => ctrl.OnLoaded(args);
