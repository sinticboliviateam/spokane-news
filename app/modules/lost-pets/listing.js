let ListingViewModel = require('~/shared/listing').ListingViewModel;

class LostPetsViewModel extends ListingViewModel
{
	constructor()
	{
		super();
		//this.endpoint = '/../wp-json/wp/v2/lostpet/';
		this.singleViewModule = 'modules/lost-pets/pet';
		//this.model.set('activityIndicator', true);
	}
	/*
	OnLoaded(args)
	{
		this.page = args.object;
		this.page.bindingContext = this.model;
		this.GetItems().then( (items) =>
		{
			this.model.activityIndicator = false;
		});
	}
	GetItems(page)
	{
		page = page || 1;
		return new Promise( (resolve, reject) =>
		{
			apiWp.ReadCustomPosts('lostpet', {page: page, limit: 50}).then( (response) =>
			{
				let res = response.content.toJSON();
				this.model.items = res;
				resolve(res);
			})
			.catch((e) =>
			{
				console.dir(e);
				reject(e);
			});
		});
	}
	*/
}
let ctrl = new LostPetsViewModel();
exports.OnLoaded = (args) => ctrl.OnLoaded(args);
