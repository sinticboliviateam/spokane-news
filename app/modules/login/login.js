var Observable 			= require("data/observable").Observable;
//var Config				= require("~/config");
var ApplicationSettings = require('application-settings');
var frameModule			= require('ui/frame');
var viewModule			= require('ui/core/view');
var observableModule 	= require("data/observable");
//var SocialLogin 		= require("nativescript-social-login");
let api                 = require('~/shared/api').api;
let base64 				= require('base-64');
let utf8 				= require('utf8');
let config				= require('~/config.js');

class LoginViewModel
{
	constructor()
	{
		this.page 			= null;
		this.model	 		= new observableModule.fromObject({
			credentials:  {
				username: config.DEVELOPMENT ? 'marcelo@mailinator.com' : '',
				password: config.DEVELOPMENT ? 'marcelo21' : ''
			},
			OnTapSignIn: (args) => this.OnTapSignIn(args),
			OnTapFacebookLogin: (args) => this.OnTapFacebookLogin(args),
			OnTapForgot: (args) => this.OnTapForgot(args),
			OnTapSignUp: (args) => this.OnTapSignUp(args)
		});
		
	}
	OnNavigatingTo(args)
	{
		this.page = args.object;
		/*
		if ( args.isBackNavigation ) 
		{
			return false;
		}
		*/
		if( ApplicationSettings.getBoolean("authenticated", false) )
		{
			frameModule.topmost().navigate({
				moduleName: 'modules/initialize/init',
				clearHistory: true
			});
			// frameModule.topmost().navigate('modules/home/home');
		}
	}
	OnLoaded(args)
	{
		
		this.page 					= args.object;
		this.page.bindingContext 	= this.model;
	}	
	/**
	 *
	 *event to signIn with mail
	 */
	OnTapSignIn(args)
	{
		try
		{
			//## ==>  yhn20mju15/	
			sbShowProgress( L('Processing...') );
			apiWp.Login(this.model.credentials).then( (result) =>
			{
				sb_hide_progress();
				let res = result.content.toJSON();
				if( result.statusCode != 200 )
					throw res;
				if( !res.token )
					throw {error: L('Authentication token error') };
		
				console.log(res);
				this.StartSession(res);
				
			})
			.catch( (e) =>
			{
				console.log(e);
				sb_hide_progress();
				alert({title: 'Authentication Error', message: 'Invalid username or password', okButtonText: 'Close'});
			});
		}
		catch(e)
		{
			sb_hide_progress();
			alert({
				title: L('Login error'),
				message: e.error || L('Unknown error, contact with support'),
				okButtonText: L('Close')
			});
		}
		
	}
	OnTapFacebookLogin(args)
	{
		try
		{
			if( args.error )
				throw {message: args.error.message};
				
			let fb_token = args.loginResponse.token;
			console.dir(`Facebook Token: ${fb_token}`);
			ApplicationSettings.setString("fb_token", fb_token);
			//let http = require('http');
			
			apiWp.post('/users/fblogin', {token: fb_token})
				.then( (r) =>
				{
					if( r.statusCode != 200 )
						throw r.content.toJSON(); //{error: 'Error trying to find the username'};
						
					let wpUser = r.content.toJSON();
					this.StartSession(wpUser);
				},
				(e) =>
				{
					console.log(e);
				});
			
			//this.StartSession();
		}
		catch(e)
		{
			console.dir(e);
			alert({title: L('Facebook Login Error'), message: e.message});
		}
	}
	OnTapForgot(args)
	{
		
		prompt({
			title: L('Forgot Password'),
			message: L('Enter the email address you used to register for "Spokane News" to reset your password.'),
			defaultText: '',
			okButtonText: L('Recover Now'),
			cancelButtonText: L('Cancel')
		}).then( (data) =>
		{
			if( data.result )
			{
				api.contentType = 'form';
				let email = base64.encode(utf8.encode(data.text));
				let params = 'email=' + email;//android.util.Base64.encodeToString(email, android.util.Base64.NO_WRAP);
				console.log(params);
				api.post('/forget_password.php', params).then( (response) =>
				{
					let res = JSON.parse(response.content);
					alert({
						title: 'Spokane News',
						message: L('Your password was successfully reset. Please check your email for instructions on choosing a new password.'),
						okButtonText: L('Ok')
					});
				},
				(e) =>
				{
					alert({
						title: 'Spokane News',
						message: L('An error ocurred while trying to recover your password, please contact with support or retry later.'),
						okButtonText: L('Close')
					});
				});
			}
		});
	}
	//evnt to redirect register page
	OnTapSignUp(args)
	{
		frameModule.topmost().navigate({
			moduleName: 'modules/register/register',
			context: 
			{
				
			},
			animated: true
		});
	}
	StartSession(user)
	{
		spk_start_session(user);
		this.page.frame.navigate({
			moduleName: 'modules/initialize/init',
			context: 
			{
				//product: product
			},
			animated: true,
			clearHistory: true
		});
	}
}
let $obj = new LoginViewModel();
exports.OnNavigatingTo = (args) => $obj.OnNavigatingTo(args);
exports.OnLoaded = (args) => $obj.OnLoaded(args);
