let frameModule 		= require('ui/frame');
let api					= require('~/shared/api').api;
let observableModule	= require('data/observable');
let ApplicationSettings	= require('application-settings');
class ContactViewModel
{
	constructor()
	{
		this.user = JSON.parse(ApplicationSettings.getString('user'));
		this.page = null;
		this.model	= observableModule.fromObject({
			subjects: [
				L('Help'),
				L('Suggestion'),
				L('Compliant')
			],
			form: {
				fullname: '',
				email: '',
				subject: '',
				message: ''
			},
			OnTapBack: (args) => this.OnTapBack(args),
			OnSubjectChanged: (args) => this.OnSubjectChanged(args),
			SendContact: (args) => this.SendContact(args)
		});
	}
	OnLoaded(args)
	{
		this.page = args.object;
		this.page.bindingContext = this.model;
	}
	OnTapBack(args)
	{
		frameModule.topmost().goBack();
	}
	OnSubjectChanged(args)
	{
		alert(args.object.selectedIndex);
	}
	SendContact(args)
	{
		
		let params = 'name=' + this.model.form.fullname +
						'&email=' + this.model.form.email +
						'&subject=' + this.model.form.subject +
						'&message=' + this.model.form.message +
						'&user_id=' + this.user.id;
		api.contentType = 'form';
		console.dir(params);
		api.post('/email_us.php', params).then( (response) =>
		{
			let res = JSON.parse(response.content);
			alert({title: 'Spokane News', message: res.api_data, okButtonText: L('Close')});
		});
	}
}
let ctrl = new ContactViewModel();
exports.OnLoaded = (args) => ctrl.OnLoaded(args); 
