const Image			= require('tns-core-modules/ui/image').Image;
const Label			= require('tns-core-modules/ui/label').Label;
const imagepicker	= require('nativescript-imagepicker');
const ImageSource	= require('tns-core-modules/image-source');
const observableModule	= require('tns-core-modules/data/observable');
const AbsoluteLayout	= require('tns-core-modules/ui/layouts/absolute-layout').AbsoluteLayout;

class SendInPicturesViewModel
{
	constructor()
	{
		this.page = null;
		this.model = observableModule.fromObject({
			message: '',
			OnTapBack: (args) => this.OnTapBack(args),
			OnTapBtnSelectImages: (args) => this.OnTapBtnSelectImages(args),
			OnTapButtonSend: (args) => this.OnTapButtonSend(args)
		});
	}
	OnLoaded(args)
	{
		this.page = args.object;
		this.images	= this.page.getViewById('images');
		this.page.bindingContext = this.model;
	}
	OnTapBack(args)
	{
		this.page.frame.navigate('modules/home/home');
	}
	OnTapBtnSelectImages(args)
	{
		let context = imagepicker.create({mode: 'multiple'});
		context.authorize().then( () =>
		{
			return context.present();
		})
		.then( (selection) =>
		{
			//console.log(JSON.stringify(selection));
			for(let sel of selection)
			{
				let image = new Image();
				image.src = sel;
				this.AppendImage(image);
			}
		});
	}
	AppendImage(image)
	{
		let layout 			= new AbsoluteLayout();
		let btnRemove 		= new Label();
		btnRemove.width 	= 25;
		btnRemove.height 	= 25;
		btnRemove.text		="&#xf057;";
		btnRemove.className ="fa btn-remove";
		btnRemove.on('tap', (data) =>
		{
			alert({title: 'Remove', message: 'Removed', okButtonText: 'Close'});
			this.images.removeChild(layout);
		});
		image.stretch = 'aspectFill';
		layout.addChild(image);
		layout.addChild(btnRemove);
		layout.className = 'selected-image-container';
		
		AbsoluteLayout.setLeft(btnRemove, 120);
		AbsoluteLayout.setTop(btnRemove, 0);
		this.images.addChild(layout);
	}
	OnTapButtonSend()
	{
		let images = [];
		this.images.eachChild( (child) =>
		{
			let img = child.getChildAt(0);
			if( img.src._android || img.src._ios )
			{
				let filename 	= img.src._android || img.src._ios;
				let extension	= filename.substr(filename.lastIndexOf('.') + 1);
				images.push({
					filename: filename,
					imageBase64: ImageSource.fromFile(filename).toBase64String(extension),
				});
			}
			return true;
		});
		sb_show_progress(L('Uploading files...'));
		let data = {
			message: this.model.message.trim(),
			images: images
		};
		apiWp.post('/../../spokanews/v1/send-pictures', data).then( (r) =>
		{
			sb_hide_progress();
			alert({title: L('Success'), message: L('You message has been sent'), okButtonText: L('Close')})
				.then( () =>
				{
					this.page.frame.navigate('modules/home/home');
				});
		},
		(e) =>
		{
			sb_hide_progress();
		});
		console.log(images);
	}
}
let ctrl = new SendInPicturesViewModel();
exports.OnLoaded = (args) => ctrl.OnLoaded(args);
