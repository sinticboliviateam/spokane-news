const observableModule = require('tns-core-modules/data/observable');

class NotificationPostViewModel
{
	constructor()
	{
		this.page = null;
		this.model = null;
	}
	OnNavigatingTo(args)
	{
		this.page = args.object;
		this.model = observableModule.fromObject({
			post: observableModule.fromObject({}),
			OnTapBack: (args) => this.OnTapBack(args)
		});
		this.model.post = this.page.navigationContext.item;
	}
	OnLoaded(args)
	{
		
		this.page.bindingContext = this.model;
	}
	OnTapBack(args)
	{
		//this.page.frame.goBack();
		this.page.frame.navigate({
			moduleName: 'modules/home/home',
			animated: true
		});
	}
}
let ctrl = new NotificationPostViewModel();
exports.OnNavigatingTo 	= (args) => ctrl.OnNavigatingTo(args);
exports.OnLoaded		= (args) => ctrl.OnLoaded(args);
