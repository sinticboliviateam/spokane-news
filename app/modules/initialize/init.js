const	ApplicationSettings	= require('application-settings');
const	observableModule	= require('tns-core-modules/data/observable');
const	fbInit				= require('nativescript-facebook').init;

class InitViewModel
{
	constructor()
	{
		this.page = null;
		this.model = observableModule.fromObject({
			init_text: L('Initializing application...')
		});
	}
	OnNavigatingTo(args)
	{
		this.page = args.object;
		fbInit('661307164269449');
		
	}
	OnLoaded(args)
	{
		this.page = args.object;
		this.page.bindingContext = this.model;
		if( !ApplicationSettings.getBoolean("authenticated", false) )
		{
			this.page.frame.navigate({
				moduleName: 'modules/login/login',
				clearHistory: true
			});
			return true;
		}
		console.log('GETTING APP SETTINGS...');
		apiWp.get('/mobile').then((r) =>
			{
				if( r.statusCode != 200 )
				{
					throw r.content.toJSON();
					return false;
				}
					
				appSettings = r.content.toJSON();
				
				//##populate app settings
				spk_setup_firebase();
				spokaneAdMob = appSettings.admob;
				//##initialize facebook
				//~ fbInit(appSettings.facebook.app_id);
				apiWc.SetCredentials(appSettings.woocommerce.consumer_key, appSettings.woocommerce.secret_key);
				spk_setup_purchases();
				//~ let route = 'modules/login/login';
				//~ let loggedin = ApplicationSettings.getBoolean('authenticated', false);
				//~ if( loggedin )
				//~ {
				let route = 'modules/home/home';
				global.spokaneUser = JSON.parse(ApplicationSettings.getString('user', '{}'));
				//~ }
				this.page.frame.navigate({
					moduleName: global.initNav && global.initNav.route ? global.initNav.route : route,
					context: global.initNav && global.initNav.context ? global.initNav.context : null,
					animated: true,
					clearHistory: true
				});
				
			})
			.catch( (e) =>
			{
				console.log('LAUNCHING ERROR');
				console.log(e);
				if( e.code == 'jwt_auth_invalid_token' )
				{
					spk_close_session();
					alert({title: L('Session Expired'), message: L('You session has expired'), okButtonText: L('Ok')})
						.then( () =>
						{
							//console.log('REDIRECTING...');
							this.page.frame.navigate({moduleName:'modules/login/login', clearHistory: true});
						});
				}
				reject(e);
			});
	}
}
let ctrl = new InitViewModel();
exports.OnNavigatingTo = (args) => ctrl.OnNavigatingTo(args);
exports.OnLoaded = (args) => ctrl.OnLoaded(args);
