const	observableModule = require('tns-core-modules/data/observable');

class AlertsViewModel
{
	constructor()
	{
		this.page = null;
		this.model = observableModule.fromObject({
			alerts: observableModule.fromObject({}),
			OnTapBack: (args) => this.OnTapBack(args),
			OnSettingChanged: (args) => this.OnSettingChanged(args),
			OnSwitchLoaded: (args) => this.OnSwitchLoaded(args),
			OnTapSave: (args) => this.OnTapSave(args)
		});
	}
	OnNavigatingTo(args)
	{
		this.page = args.object;
		if(this.page.navigationContext && this.page.navigationContext.alerts)
		{
			Object.assign(this.model.alerts = this.page.navigationContext.alerts);
			console.log(this.model.alerts);
		}
	}
	OnLoaded(args)
	{
		this.page = args.object;
		this.page.bindingContext = this.model;
	}
	OnSwitchLoaded(args)
	{
		args.object.on('checkedChange', (args) => this.OnSettingChanged(args));
	}
	OnTapBack(args)
	{
		this.page.frame.navigate({
			moduleName: 'modules/settings/main',
			context: {
				from: 'alerts',
				data: this.model.alerts
			},
			animated: true
		});
	}
	OnSettingChanged(args)
	{
		let property = args.object.key;
		this.model.alerts[property] = args.object.checked;
		//console.log(this.model.alerts);
	}
	OnTapSave(args)
	{
		this.OnTapBack(args);
	}
}
let ctrl = new AlertsViewModel();
exports.OnNavigatingTo = (args) => ctrl.OnNavigatingTo(args);
exports.OnLoaded = (args) => ctrl.OnLoaded(args);
exports.OnSwitchLoaded = (args) => ctrl.OnSwitchLoaded(args);
