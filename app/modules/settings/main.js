let applicationSettings = require('tns-core-modules/application-settings');
let observableModule	= require('data/observable');

class SettingsViewModel
{
	constructor()
	{
		this.page = null;
		this.model = observableModule.fromObject({
			settings: observableModule.fromObject({
				alerts: {},
				notifications: {}
			}),
			OnTapBack: (args) => this.OnTapBack(args),
			OnTapCustomizeAlerts: (args) => this.OnTapCustomizeAlerts(args),
			OnTapAppVersion: (args) => this.OnTapAppVersion(args)
		});
	}
	OnNavigatingTo(args)
	{
		this.page = args.object;
		let context = this.page.navigationContext;
		
		
		if( context && context.from )
		{
			switch(context.from)
			{
				case 'alerts':
					//Object.assign(this.model.settings.alerts, context.data);
					this.model.settings.alerts = context.data;
				break;
				case 'notifications':
					//Object.assign(this.model.settings.notifications, context.data);
					this.model.settings.notifications = context.data;
				break;
			}
			//##save send settings to server
			this.SaveSettings().then( (settings) =>
			{
				//~ console.log('NEW SETTINGS');
				//~ console.log(settings);
				spokaneUser.settings = settings;
				applicationSettings.setString('user', JSON.stringify(spokaneUser))
			});
		}
		else
		{
			this.GetSettings();
		}
	}
	OnLoaded(args)
	{
		this.page = args.object;
		this.page.bindingContext = this.model;
	}
	OnTapBack(args)
	{
		this.page.frame.navigate('modules/home/home');
	}
	OnTapCustomizeAlerts(args)
	{
		this.page.frame.navigate({
			moduleName: 'modules/settings/alerts',
			context: {alerts: this.model.settings && this.model.settings.alerts ? this.model.settings.alerts : {}},
			animated: true
		});
	}
	OnTapAppVersion(args)
	{
		let version = "Version: 1.0.37\n";
		if( android )
			version += "Platform: Android OS";
		else
			version += "Platform: iOS";
			
		alert({title: L('About Application'), message: version, okButtonText: L('Close')});
	}
	GetSettings()
	{
		if( spokaneUser.settings )
			this.model.settings = spokaneUser.settings;
		console.log(this.model.settings);
		
		/*
		return new Promise( (resolve, reject) =>
		{
			
		});
		*/
	}
	SaveSettings()
	{
		return new Promise( (resolve, reject) =>
		{
			let data = this.model.settings;
			//console.log('SAVING SETTINGS');
			//console.log(this.model.settings);
			apiWp.post('/settings', data)
				.then( (r) =>
				{
					if( r.statusCode != 200 )
						throw r.content.toJSON();
						
					let res = r.content.toJSON();
					//~ console.log('SETTINGS SAVED');
					//~ console.log(res);
					resolve(res);
				},
				(e) =>
				{
					console.log('SETTINGS ERROR');
					console.log(e);
					alert({title: L('Save Settings ERROR'), message: L('Error trying to save settings')});
					reject(e);
				});
		});
	}
}
let ctrl = new SettingsViewModel();
exports.OnNavigatingTo = (args) => ctrl.OnNavigatingTo(args);
exports.OnLoaded = (args) => ctrl.OnLoaded(args);
exports.OnTapBack = (args) => ctrl.OnTapBack(args);
