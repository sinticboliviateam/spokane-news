const observableModule	= require('tns-core-modules/data/observable');
const Menu				= require('nativescript-menu').Menu;
const frameModule		= require('tns-core-modules/ui/frame');
class MyBuySellViewModel
{
	constructor()
	{
		this.component = null;
		this.model = observableModule.fromObject({
			items: [],
			OnTapPopupMenu: (args) => this.OnTapPopupMenu(args)
		});
	}
	OnLoaded(args)
	{
		this.component = args.object;
		this.component.bindingContext = this.model;
		this.GetItems();
	}
	GetItems()
	{
		apiWp.get('/buysell' + sb_object2querystring({author: spokaneUser.id, status: 'any'}))
			.then( (r) =>
			{
				if( r.statusCode != 200 )
					throw r.content.toJSON();
				let items = r.content.toJSON();
				this.SetItems(items);
			})
			.catch( (e) =>
			{
				alert({title: L('Error'), message: e.message || e.error || L('Unknown error'), okButtonText: L('Close')});
			});
	}
	SetItems(items)
	{
		this.model.items = items;
	}
	OnTapPopupMenu(args)
	{
		Menu.popup({view: args.object, actions: [L('Edit'), L('Delete')]})
			.then( (value) =>
			{
				if( value == L('Delete') )
				{
					this.Delete(args.object.pid);
				}
				else if( value == L('Edit') ) 
				{
					frameModule.topmost().navigate({
						moduleName: 'modules/buyandsell/new-ad',
						context: {id: args.object.pid },
						animated: true
					});
				}
			})
			.catch( (e) =>
			{
				console.log('ERROR');
				console.log(e);
			});
	}
	Delete(productId)
	{
		confirm(L('Are you sure to delete the post?'))
			.then( ( result ) =>
			{
				if( result )
				{
					sbShowProgress(L('Deleting post...'));
					apiWc.DeleteProduct(productId )
						.then( (r) =>
						{
							spokaneLoading.hide();
							if( r.statusCode != 200 )
								throw r.content.toJSON();
							alert({title: L('Deletion'), message: L('The post has been deleted'), okButtonText: L('Close')});
						})
						.catch( (e) =>
						{
							spokaneLoading.hide();
							alert({
								title: L('Deletion Error'),
								message: e.message || e.error || L('An error ocurred whilte trying to delete the post, try it later'),
								okButtonText: L('Close')
							});
						});
				}
			});
	}
}
let ctrl = new MyBuySellViewModel();
exports.OnLoaded = (args) => ctrl.OnLoaded(args);
