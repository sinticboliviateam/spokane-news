const observableModule = require('tns-core-modules/data/observable');
const Menu				= require('nativescript-menu').Menu;
const frameModule		= require('tns-core-modules/ui/frame');
class MyMissingPersonsViewModel
{
	constructor()
	{
		
		this.page = null;
		this.listview = null;
		this.model = observableModule.fromObject({
			items: [],
			OnTapPopupMenu: (args) => this.OnTapPopupMenu(args)
		});
		
	}
	OnLoaded(args)
	{
		this.page = args.object;
		this.listview = this.page.getViewById('listview-items');
		this.page.bindingContext = this.model;
		this.GetItems();
	}
	OnTapPopupMenu(args)
	{
		Menu.popup({view: args.object, actions: [L('Edit'), L('Delete')]})
			.then( (value) =>
			{
				console.log(value);
				if( value == L('Delete') )
				{
					this.DeletePerson(args.object.pid);
				}
				else if( value == L('Edit') ) 
				{
					frameModule.topmost().navigate({
						moduleName: 'modules/missing-persons/report-person',
						context: {id: args.object.pid },
						animated: true
					});
				}
			})
			.catch( (e) =>
			{
				console.log('ERROR');
				console.log(e);
			});
	}
	GetItems()
	{
		apiWp.ReadCustomPosts('missing_person', {author: spokaneUser.id, status: 'any'})
			.then( (r) =>
			{
				if( r.statusCode != 200 )
					throw r.content.toJSON();
					
				let res = r.content.toJSON();
				this.model.items = res;
				//this.listview.notifyPullToRefreshFinished();
			})
			.catch( (e) =>
			{
				console.log(e);
				alert({title: L('Error'), message: L('Error retrieving missing persons'), okButtonText: L('Close')});
			});
	}
	DeletePerson(personId)
	{
		confirm(L('Are you sure to delete the post?'))
			.then( ( result ) =>
			{
				console.log(result);
				console.log(result == true);
				if( result )
				{
					sbShowProgress(L('Deleting post...'));
					apiWp.DeleteCustomPost(personId, 'missing_person')
						.then( (r) =>
						{
							spokaneLoading.hide();
							if( r.statusCode != 200 )
								throw r.content.toJSON();
							alert({title: L('Deletion'), message: L('The post has been deleted'), okButtonText: L('Close')});
						})
						.catch( (e) =>
						{
							spokaneLoading.hide();
							alert({
								title: L('Deletion Error'),
								message: e.message || e.error || L('An error ocurred whilte trying to delete the post, try it later'),
								okButtonText: L('Close')
							});
						});
				}
			});
	}
}
let ctrl = new MyMissingPersonsViewModel();
exports.OnLoaded = (args) => ctrl.OnLoaded(args);
