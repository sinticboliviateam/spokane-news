const observableModule = require('tns-core-modules/data/observable');
class MyPostsViewModel
{
	constructor()
	{
		this.page = null;
		this.model = observableModule.fromObject({
			OnTapBack: (args) => this.OnTapBack(args)
		});
	}
	OnNavigatingTo(args)
	{
		
	}
	OnLoaded(args)
	{
		this.page = args.object;
		this.page.bindingContext = this.model;
	}
	OnTapBack(args)
	{
		this.page.frame.navigate({
			moduleName: 'modules/home/home',
			animated: true
		});
	}
}
let ctrl = new MyPostsViewModel();
exports.OnNavigatingTo = (args) => ctrl.OnNavigatingTo(args);
exports.OnLoaded = (args) => ctrl.OnLoaded(args);
