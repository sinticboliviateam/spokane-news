const observableModule = require('tns-core-modules/data/observable');
const Menu				= require('nativescript-menu').Menu;
const frameModule		= require('tns-core-modules/ui/frame');

class MyLostFoundPetsViewModel
{
	constructor()
	{
		console.log('MyLostFoundPetsViewModel');
		this.page = null;
		this.model = observableModule.fromObject({
			items: [],
			OnTapPopupMenu: (args) => this.OnTapPopupMenu(args)
		});
	}
	OnLoaded(args)
	{
		this.page = args.object;
		this.page.bindingContext = this.model;
		console.log('Getting posts...');
		apiWp.ReadCustomPosts('lostpet', {author: spokaneUser.id, status: 'any'})
			.then( (r) =>
			{
				if( r.statusCode != 200 )
					throw r.content.toJSON();
				let res = r.content.toJSON();
				console.log(res);
				this.FillItems(res);
			})
			.catch( (e) =>
			{
				console.log(e);
				alert({title: L('ERROR'), message: L('Error trying to retrive your posts'), okButtonText:L('Close')});
			});
	}
	FillItems(items)
	{
		this.model.items = items;
	}
	OnTapPopupMenu(args)
	{
		Menu.popup({view: args.object, actions: [L('Edit'), L('Delete')]})
			.then( (value) =>
			{
				if( value == L('Delete') )
				{
					this.Delete(args.object.pid);
				}
				else if( value == L('Edit') )
				{
					frameModule.topmost().navigate({
						moduleName: 'modules/lost-pets/report-pet',
						context: {id: args.object.pid },
						animated: true
					});
				}
			})
			.catch( (e) =>
			{
				console.log(e);
			});
	}
	Delete(petId)
	{
		confirm(L('Are you sure to delete the post?'))
			.then( ( result ) =>
			{
				if( result )
				{
					sbShowProgress.show({message: L('Deleting post...')});
					apiWp.DeleteCustomPost(petId)
						.then( (r) =>
						{
							if( r.statusCode != 200 )
								throw r.content.toJSON();
							alert({title: L('Deletion'), message: L('The post has been deleted'), okButtonText: L('Close')});
						})
						.catch( (e) =>
						{
							alert({title: L('Deletion Error'), message: L('An error ocurred whilte trying to delete the post, try it later'), okButtonText: L('Close')});
						});
				}
			});
	}
}
let ctrl = new MyLostFoundPetsViewModel();
exports.OnLoaded = (args) => ctrl.OnLoaded(args);
