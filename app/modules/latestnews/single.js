let observableModule 	= require('data/observable');
let frameModule			= require('ui/frame');
let api					= require('~/shared/api').api;
let GridLayoutModule	= require('tns-core-modules/ui/layouts/grid-layout');
let StackLayout			= require('tns-core-modules/ui/layouts/stack-layout').StackLayout;
let Label				= require('tns-core-modules/ui/label').Label;
let Image				= require('tns-core-modules/ui/image').Image;
let FormattedString		= require('text/formatted-string').FormattedString;
let SpanModule			= require('text/span');
const SBCommentsModule	= require('~/shared/comment/sb-comment');

class SingleViewModel
{
	constructor()
	{
		this.page 			= null;
		this.model			= null;
		this.commentsPage 	= 0;
		this.container		= null;
		this.scrollView		= null;
		this.css			= '<style>img{max-width:50%;max-height:150px;}</style>';
		this.contentType	= null;
		this.model = observableModule.fromObject({
			post: {},
			comments: [ ],
			comment: '',
			activityIndicator: true,
			showButtonMore: true,
			OnTapBack: (args) => this.OnTapBack(args),
			OnTapPicture: (args) => this.OnTapPicture(args),
			OnTapLoadMoreComments: (args) => this.OnTapLoadMoreComments(args),
			OnTapButtonSendComment: (args) => this.OnTapButtonSendComment(args),
			OnCreatingViewVideo: (args) => this.OnCreatingViewVideo(args)
		});
	}
	OnNavigatingTo(args)
	{
		this.page = args.object;
		if( this.page.navigationContext && this.page.navigationContext.id )
		{
			this.contentType = this.page.navigationContext.type || 'news';
			this.model.post.id = this.page.navigationContext.id;
			this.GetPost(this.model.post.id, this.contentType);
		}
		else if( this.page.navigationContext && this.page.navigationContext.post )
		{
			this.contentType = this.page.navigationContext.post.type == 'fbpost' ? 'news' : '';
			this.SetPostData(this.page.navigationContext.post);
		}
	}
	OnLoaded(args)
	{
		sb_admob_interstitial();
		this.commentsPage 	= 1;
		this.page 			= args.object;
		this.container 		= this.page.getViewById('listing-comments');
		this.scrollView		= this.page.getViewById('scroll-single');
		this.videoContainer	= this.page.getViewById('video-container');
		this.page.bindingContext = this.model;
		this.CheckForVideo();
		this.GetComments(this.commentsPage)
			.then( (items) =>
			{
				this.FillComments(items, true);
			});
	}
	OnTapBack()
	{
		let tabKey = 'latestnews';
		if( this.model.post.type == 'post' )
			tabKey = 'media_releases';
		
		//frameModule.topmost().goBack();
		this.page.frame.navigate({
			moduleName: 'modules/home/home',
			context: {tab: tabKey}
		});
	}
	OnTapPicture(args)
	{
		let context = {
			images: [
				{src: args.object.src, title: '', text: ''}
			]
		};
		this.page.showModal('modules/image-viewer/multiple', context, () => {}, !true);
	}
	OnTapLoadMoreComments(args)
	{
		this.GetComments(this.commentsPage + 1)
			.then( (items) =>
			{
				this.FillComments(items);
			});
	}
	OnTapButtonSendComment(args)
	{
		let theComment = this.model.comment.trim();
		this.model.comment = '';
		this.page.getViewById('comment-input-text').dismissSoftInput();
		this.scrollView.scrollToVerticalOffset(this.scrollView.scrollableHeight, false);
		apiWp.CreateComment(this.model.post.id, theComment)
			.then( (r) =>
			{
				args.object.editable = true;
				let res = r.content.toJSON();
				if( r.statusCode != 201 )
					throw res;
				this.AppendComment(res);
				this.scrollView.scrollToVerticalOffset(this.scrollView.scrollableHeight, false);
			})
			.catch( (e) =>
			{
				args.object.editable = true;
				this.model.comment = theComment;
				alert({
					title: L('Comment Error'),
					message: L('Error trying to send your comment, try it later.'),
					okButtonText: L('Close')
				});
			});
	}
	GetPost(id, type)
	{
		return new Promise( (resolve, reject) =>
		{
			if( type == 'post' )
			{
			}
			else
			{
				apiWp.ReadCustomPost(id, 'fbpost')
					.then( (r) =>
					{
						if( r.statusCode != 200 )
							throw r.content.toJSON();
						let data = r.content.toJSON();
						this.SetPostData(data);
						resolve(data);
					})
					.catch( (e) =>
					{
						reject(e);
					});
			}
		});
	}
	SetPostData(post)
	{
		//post.content.rendered += this.css;
		post.content.rendered = '<font color="black">' + sb_n2br(sb_striptags(post.content.rendered.trim()), 2) + '</font>';			
		this.model.post = post;
		//~ if( this.model.post.meta._type == 'video' && this.model.post.meta._video_url )
		//~ {
			//~ let videoPlayerModule = require('nativescript-videoplayer').Video;
		//~ }
	}
	CheckForVideo()
	{
		if( this.contentType != 'news' )
			return false;
		
		if( !this.model.post.meta || this.model.post.meta._type == 'video' )
		{
			this.GetPost(this.model.post.id, 'news').then((post) =>
			{
				let videoPlayerModule = require('nativescript-videoplayer').Video;
				let player = new videoPlayerModule();
				player.src = post.meta._video_url;
				player.controls = true;
				player.autoplay = false;
				player.loop = false;
				player.height = 280;
				//player.fill		= true;
				player.className = 'video-player';
				this.videoContainer.addChild(player);
			});
		}
	}
	GetComments(page)
	{
		return new Promise( (resolve, reject) =>
		{
			this.model.activityIndicator = true;
			apiWp.ReadAllComments(this.model.post.id, page)
				.then( (result) =>
				{
					let totalRows	= parseInt(result.headers['X-WP-Total']);
					let totalPages 	= parseInt(result.headers['X-WP-TotalPages']);
					//console.log(`totalPages = ${totalPages}, totalRows: ${totalRows}, page = ${page}`);
					this.model.activityIndicator = false;
					if( result.statusCode != 200 )
						throw result.content.toJSON();
						
					let items = result.content.toJSON();
					if( items.length <= 0 )
					{
						this.model.showButtonMore = false;
						return true;
					}
					else
					{
						this.model.showButtonMore = page < totalPages;
						
						//this.commentsPage++;
						this.commentsPage = page;
						resolve(items);
					}
					
				})
				.catch( (e) =>
				{
					this.model.activityIndicator = false;
					console.log('ERROR: Getting comments');
					reject(e);
				});
		});
	}
	FillComments(comments, clear)
	{
		if( clear )
			this.container.removeChildren();
		for(let comment of comments)
		{
			try
			{
				this.AppendComment(comment);
			}
			catch(e)
			{
				console.log(e);
			}
		}
	}
	AppendComment(comment)
	{
		//console.dir(comment);
		let c = new SBCommentsModule.SBComment();
		c.model.isHtml = true;
		c.SetComment({
			id: comment.id,
			author: {
				id:		parseInt(comment.author),
				image:	comment.author_picture || comment.meta._fb_author_picture || comment.author_avatar_urls['48'],
				name: 	comment.author_name
			},
			content: sb_replace_html_numbers(comment.content.rendered.trim()),
			date_time: sb_formatgmtdate(comment.date_gmt, 'M d Y H:i:s')
		});
		/*
		c.viewComponent.bindingContext.showActions = true;
		if( comment.author == spokaneUser.id )
		{
			
			c.viewComponent.bindingContext.showDelete = true;
			c.SetDeleteEvent( () =>
			{
				console.log(`DELETING COMMENT => ${comment.id}`);
				this.DeleteComment(comment.id, c);
			});
			
		}
		*/
		this.container.addChild(c);
		
	}
	DeleteComment(id, view)
	{
		apiWp.DeleteComment(id)
			.then( (r) =>
			{
				//console.dir(r);
				this.container.removeChild(view);
			})
			.catch( (e) =>
			{
			});
	}
}
let ctrl = new SingleViewModel();
exports.OnNavigatingTo = (args) => ctrl.OnNavigatingTo(args);
exports.OnLoaded = (args) => ctrl.OnLoaded(args);
exports.OnCreatingViewVideo = (args) => ctrl.OnCreatingViewVideo(args);
