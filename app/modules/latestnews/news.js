let	applicationSettings = require('application-settings');
let api                 = require('~/shared/api').api;
let	observableModule	= require('data/observable');
//let     application		= require('application');
let frameModule			= require('ui/frame');
let admob 				= require("nativescript-admob");
const ObservableArray	= require('tns-core-modules/data/observable-array').ObservableArray;
class NewsViewModel
{
	constructor()
	{
		console.log('constructor -> NewsViewModel');
		this.page 				= null;
		this.model 				= null;
		this.groupItemsCount 	= 0;
		this.itemsPage			= 0;
		this.totalPages			= 0;
		this.totalRows			= 0;
		this.blockActivity		= false;
		
		this.model = observableModule.fromObject({
			news: new ObservableArray([]),
			RefreshNews: (args) => this.RefreshNews(args),
			LoadMore: (args) => this.LoadMore(args),
			OnTapNews: (args) => this.OnTapNews(args),
			templateSelector: (args) => this.templateSelector(args),
			itemLoadingEvent: (args) => this.itemLoadingEvent(args),
			creatingViewAdSense: (args) => this.creatingViewAdSense(args),
			showActivity: true
		});
		
	}
	OnNavigatingTo(args)
    {
		this.groupItemsCount = 0;
	}
	OnLoaded(args)
    {
        this.page               	= args.object;
        this.listview				= this.page.getViewById("news-listing");		
        this.page.bindingContext 	= this.model;
        global.NotifyNewsRefresh	= () => {this.RefreshNews();};
        this.RefreshNews();
    }
    GetNews(page)
	{
		return new Promise( (resolve, reject) =>
		{
			apiWp.ReadCustomPosts('fbpost', {page: page, limit: 50})
				.then( (r) =>
				{
					this.model.showActivity = false;
					//let res = r;
					//console.dir(response.headers);
					//console.log(`StatusCode => ${response.statusCode}`);
					if( r.statusCode != 200 )
						throw r.content.toJSON();
					this.itemsPage = page;
					//let items = r.content.toJSON();
					
					resolve(r);
				})
				.catch( (e) =>
				{
					//console.dir(e);
					this.model.showActivity = false;
					alert({
						title: 'Error Loading items',
						message: 'An error ocurred whilte trying to load items',
						okButtonText: 'Close'
					});
					reject(e);
				});
		});
	}
	RefreshNews(args)
	{
		this.GetNews(1)
			.then( (response) =>
			{
				this.totalRows	= parseInt(response.headers['X-WP-Total']);
				this.totalPages = parseInt(response.headers['X-WP-TotalPages']);
				this.model.news = new ObservableArray(response.content.toJSON());
				this.listview.notifyPullToRefreshFinished();
			})
			.catch( (e) =>
			{
				this.listview.notifyPullToRefreshFinished();
			});
			
		if( args )
			args.returnValue = true;
	}
	LoadMore(args)
	{
		//console.log('LoadMore');
		if( this.itemsPage + 1 >= this.totalPages )
		{
			this.listview.notifyLoadOnDemandFinished();
			return false;
		}
		this.GetNews(this.itemsPage + 1)
			.then( (response) =>
			{
				this.model.news.push(response.content.toJSON());
				this.listview.notifyLoadOnDemandFinished();
				
			})
			.catch( (e) =>
			{
				this.listview.notifyLoadOnDemandFinished();
			});
		args.returnValue = true;
	}
	OnTapNews(args)
	{
		//let post = this.model.news[args.index];
		let post = this.model.news.getItem(args.index);
		if( !post )
			return false;
		//console.log(frameModule.topmost().constructor.name);
		//console.log(frameModule.topmost().parent.parent.page.constructor.name);
		let frame = frameModule.topmost();//.parent.parent.page.frame;
		frame.navigate({
			moduleName: 'modules/latestnews/single',
			context: 
			{
				post: post
			},
			animated: true
		});
		
	}
	templateSelector(item)
	{
		//~ this.groupItemsCount++;
		//~ if( this.groupItemsCount == 1 )
			//~ return 'admob-template';
	}
	itemLoadingEvent(args)
	{
		/*
		if( args.index == '0' )
		{
			setTimeout(() => this.AdSense(this.page.getViewById('admob-item')), 1000);
		}
		*/
		//console.dir(args);
	}
	creatingViewAdSense(args)
	{
		//console.log('AdMob PLACEHOLDER CREATING VIEW');
		//let placeholder = this.page.getViewById('admob-item');
		//console.dir(args);
		/*
		admob._getActivity = function ()
		{
			console.log('GETTING ADMOB ACTIVITY');
			admob.activity  = args.object._context;
			return admob.activity;
			//
			//if (admob.activity === null)
			//{
			//	admob.activity = application.android.foregroundActivity;
			//}
			//return admob.activity;
			//
		};
		*/
		//this.AdSense();
	}
	GetPlaceholder()
	{
		return this.page.getViewById('admob-item');
	}
}
let ctrl = new NewsViewModel();
exports.OnNavigatingTo  = (args) => ctrl.OnNavigatingTo(args);
exports.OnLoaded        = (args) => ctrl.OnLoaded(args);
exports.OnPageUnloaded  = (args) => ctrl.OnPageUnLoaded(args);
