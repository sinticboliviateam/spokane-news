/*
In NativeScript, the app.js file is the entry point to your application.
You can use this file to perform app-level initialization, but the primary
purpose of the file is to pass control to the app’s first module.
*/

require("./bundle-config");
require('nativescript-i18n');
require('~/shared/functions');
require('globals');

const application 		= require("tns-core-modules/application");
var frameModule			= require('ui/frame');
let	ApplicationSettings	= require('application-settings');

global.spokaneAdMob			= {};
global.apiWp				= require('~/shared/api-wp').GetInstance();
global.apiWc				= require('~/shared/api-woocommerce').GetInstance();
global.spokaneProducts 		= {};
global.appSettings			= {};
global.spokaneUser			= null;
global.resuming				= false;
global.overrideInitRoute	= null;

application.on(application.launchEvent, (args) =>
{
	console.log('LAUNCHING APPLICATION...');
});
application.on(application.suspendEvent, (args) =>
{
	console.log('SUSPENDING APPLICATION');
    if (args.android)
    {
        // For Android applications, args.android is an android activity class.
        console.log("Activity: " + args.android);
    }
    else if (args.ios)
    {
        // For iOS applications, args.ios is UIApplication.
        console.log("UIApplication: " + args.ios);
    }
});
application.on(application.resumeEvent, (args) =>
{
	resuming = true;
	console.log('RESUMING APPLICATION');
    if (args.android)
    {
        // For Android applications, args.android is an android activity class.
        console.log("Activity: " + args.android);
    }
    else if (args.ios)
    {
        // For iOS applications, args.ios is UIApplication.
        console.log("UIApplication: " + args.ios);
    }
});
spk_setup_notifications();
application.start({ moduleName: 'modules/initialize/init' });
//application.run({ moduleName: "modules/login/login" });

/*
Do not place any code after the application has been started as it will not
be executed on iOS.
*/
