#!/bin/bash
touch ~/.android/repositories.cfg
sdkmanager "platform-tools" "platforms;android-28"
sdkmanager  "build-tools;28.0.2" "extras;google;m2repository" "extras;android;m2repository"
sdkmanager "system-images;android-28;google_apis_playstore;x86"
avdmanager list device
android list targets
avdmanager create avd -n "DeviceMobile" -d 5 -k "system-images;android-28;google_apis_playstore;x86" 
