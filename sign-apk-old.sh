#!/bin/bash
ALIAS=spokane
KEYSTORE=resources/old-keys/SpokaneKey.jks
PWD=spokanenews
OUTPUT_PATH=platforms/android/app/build/outputs
SDK_PATH=~/secondary/programs/android-sdk-linux

if [ "$1" = "keystore" ]; then
	keytool -genkey -v -keystore $ALIAS.keystore -alias $ALIAS -keyalg RSA -keysize 2048 -validity 10000
	keytool -importkeystore -srckeystore $ALIAS.keystore -destkeystore $ALIAS.keystore -deststoretype pkcs12
elif [ "$1" = "bundle" ]; then
	tns build android --release --key-store-path $KEYSTORE --key-store-password $PWD --key-store-alias $ALIAS --key-store-alias-password $PWD --aab --copy-to "${OUTPUT_PATH}/bundle/release/${ALIAS}-bundle.aab"
elif [ "$1" = "sign-bundle" ]; then
	jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore $KEYSTORE "${OUTPUT_PATH}/bundle/release/${ALIAS}-bundle.aab" $ALIAS
elif [ "$1" = "release" ]; then
	tns build android --release --key-store-path $KEYSTORE --key-store-password $PWD --key-store-alias $ALIAS --key-store-alias-password $PWD
elif [ "$1" = "sign" ]; then
	jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore $KEYSTORE "${OUTPUT_PATH}/apk/release/${ALIAS}.apk" $ALIAS
	#jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore $KEYSTORE "${ALIAS}-bundle.aab" $ALIAS
elif [ "$1" = "align" ]; then
	${SDK_PATH}/build-tools/28.0.2/zipalign -v 4 ${OUTPUT_PATH}/apk/release/${ALIAS}-bundle.apk ${OUTPUT_PATH}/${ALIAS}-aligned.apk
elif [ "$1" = "align-bundle" ]; then
	${SDK_PATH}/build-tools/28.0.2/zipalign -v 4 ${OUTPUT_PATH}/bundle/release/${ALIAS}-bundle.aab ${OUTPUT_PATH}/bundle/release/${ALIAS}-bundle-aligned.aab
fi

